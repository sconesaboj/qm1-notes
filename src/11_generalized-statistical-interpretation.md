---
title: Generalized Statistical Interpretation
---

**Week 4, lecture 12**

!!! summary "Learning objectives"

    After following this lecture, you will be able to:

    - Determine what are the **outcomes of a possible measurement **of a given physical observable.
    - Utilise the generalized statistical interpretation to determine the **probabilities** associated to the outcome of measurements in quantum systems.
    - Assess how the effect of a measurement changes a quantum state by means of the **collapse** of its wave function.

In previous lectures, you have learned about the statistical interpretation of the wavefunction $\psi(x)$ of a quantum system. You have seen how the square of the wave function, $|\psi(x)|^2$, can be understood as the probability distribution of the particle's position $x$.

To be more precise, if the quantum state of a given particle is characterized by the wave function $\psi(x)$. then we know that $P(x)=|\psi(x)|^2dx$ represents the **probability** $P(x)$ of finding the particle in the region of space defined by the infinitesimal interval $[x,x+dx]$. 

This implies, among other properties, that the global maximum of $|\psi(x)|^2$ is the **most likely value** to find a particle upon a measurement of its position.

Let us assume now that we have a particle characterized by the time-dependent wavefunction $\Psi(x,t)$ and that we want to measure some physical observable denoted by $Q$. For example, this observable could be the position ($Q=x$), the linear momentum ($Q=p_x$), or the total energy ($Q=E$) of this particle. It could also be the $z$-component of the spin of an electron, $Q=S_z$.

We have seen that this physical observable $Q$ will be represented by an Hermitian operator $\hat{Q}$ acting on the state vectors of the Hilbert space of this system. Furthermore, we know that the possible outcomes of a measurement of $Q$ in **a general quantum system** will be given by the **eigenvalues of $\hat{Q}$**. 

The question that we will answer in this lecture is the following. We know already that solving the eigenvalue equation for $\hat{Q}$

$$
\hat{Q}\psi_l(x) = q_l \psi_l(x)
$$

with $l$ being an integer quantum number that labels the eigenvector, tells us that a measurement of $Q$ can only have as outcome one of the eigenvalues $\{q_l\}$. 

But then, what is the **probability** of measuring one specific eigenvalue, say $P(q_1)$, in the quantum state $\Psi(x,t)$?

To answer this question, we are going to state another of the **fundamental axioms** that define the quantum mechanics, which goes under the name of the **generalised statistical interpretation**. 

This axiom allows one to calculate the probabilities associated to the outcome of specific measurements in a quantum system, and generalises the probabilistic interpretation of the wave function that we have been using so far.

# The generalized statistical interpretation

Let us consider here the discrete case, relevant for physical observables $Q$ whose Hermitian operator has associated a **discrete** (rather than continuous) eigenvalue spectrum. 

!!! warning
    Note that having a discrete spectrum does not imply a _finite_ spectrum. In general, we can have Hermitian operators with a **discrete spectrum** and also **a infinite number of eigenvalues**.   For example, both the infinite square well and the quantum harmonic oscillators have quantised energies and eigenfunctions, but there exist an infinite number of them.   

Since we have demonstrated that the eigenvectors of the Hermitian operator $\hat{Q}$ form a **complete basis**, we know that we can always expand out wave function as a linear superposition of these eigenvectors:

$$\Psi(x,t)=\sum_n c_l(t) \psi_l(x)~~{\rm Eq.}~[1]$$

where $\hat{Q}\psi_l(x) = q_l \psi_l(x)$ is the relevant eigenvalue equation. 

The coefficients $c_l(t)$ in Eq. [1] are complex scalars, and can be determined by exploiting the orthonormality property of the eigenvectors of an Hermitian operator. 

To demonstrate this result, one should start from Eq. [1], multiply it by $\psi^*_m(x)$, and then integrate over all values of $x$:

$$
\int_{-\infty}^{\infty}dx~\psi^*_m(x)\Psi(x,t)= \int_{-\infty}^{\infty}dx~\psi^*_m(x)\left(\sum_l c_l(t) \psi_l(x)\right)
$$

and now in the right-hand side of the equation use that the coefficients do not depend on $x$ and that the eigenfunctions are orthonormal to obtain:

$$
\int_{-\infty}^{\infty}dx~\psi^*_m(x)\left(\sum_l c_l(t) \psi_l(x)\right) = \sum_l c_l(t)\int_{-\infty}^{\infty}dx~\psi^*_m(x)\psi_l(x) =
$$

$$
\sum_l c_l(t) \delta_{lm} = c_m(t)
$$

So therefore we find that the coefficients $c_n(t)$ are determined by the **overlap integral** between the total wave function and the eigenvector associated to the eigenvalue $q_n$:

$$
c_n(t) = \int_{-\infty}^{\infty}dx~\psi^*_n(x)\Psi(x,t)
$$

The larger the overlap between $\psi_n(x)$ and $\Psi(x,t)$, in other words, the **more similar **that $\psi_n(x)$ is to $\Psi(x,t)$, the larger the overlap integral will be and the larger the coefficient $c_n(t)$ will be.

Conversely, if the eigenfunction $\psi_n(x)$ does not overal with the total wave function $\Psi(x,t)$) (for example if the two functions are very different) then the associated coefficient will vanish, $c_n(t)=0$.

Now we have all the ingredients to present the generalized statistical interpretation of quantum mechanics (remember, it is an **axiom** of the theory):

- Assume that we want to measure the observable $Q$, represented by the Hermitian operator $\hat{Q}$ with eigenvalues and eigenvectors $\hat{Q}\psi_l(x)=q_l\psi_l(x)$.
- The only possible outcomes of this measurement will be one of the eigenvalues $\{q_l\}$
- The probability of measuring the eigenvalue $q_n$, $P(q_n)$, will be given by
$$
P(q_n) = |c_n(t)|^2 \, ~~{\rm with} ~~c_n(t) = \int_{-\infty}^{\infty}dx~\psi^*_n(x)\Psi(x,t)~~{\rm Eq.}~[2]
$$
that is, the probability $P(q_n)$ is the square of the coefficient $c_n$ of the linear expansion of $\Psi(x,t)$ in terms of the eigenfunctions of $\hat{Q}$
$$
\Psi(x,t)=\sum_l c_l(t) \psi_l(x)
$$
- The larger the value of the overlap integral between $\psi^*_n(x)$ and $\Psi(x,t)$, the larger the probability $P(q_n)$ for measuring the eigenvalue $q_n$.

This way, if we know the eigenvalues and eigenfunctions of the Hermitian operator $\hat{Q}$, we can always evaluate the probability of finding the result $q_n$ when measuring the observable $Q$ in the quantum state $\Psi(x,t)$ by evaluating the overlap integral Eq. [2]


!!! important

    Upon this measurement of the observable $Q$, the outcome will be one of the eigenvalues of $\hat{Q}$, say $q_n$. After the measurement,  the quantum state **collapses** into the corresponding eigenfunction $\psi_n(x)$. That is, if **before the measurement** of $Q$ at $t=t_m$ the wave function of the system is given by

    $$\Psi(x,t_m)=\sum_l c_l(t_m) \psi_l(x)~~{\rm (before)}$$

    then **after the measurement** the same wave function will have transformed into the eigenfunction $\psi_n(x)$ associated to the eigenvalue $q_m$ which has just been measured

    $$\Psi(x,t_m)=\psi_n(x)~~{\rm (after)}$$

    Therefore, after the measurement of $Q$ the resulting quantum state will always be a **determinate state** of $Q$. And thus if at another time $t>t_m$ we measure again the observable $Q$ in this same quantum state, it is ** guaranteed** that we will find the outcome $q_n$.

This axiom of quantum mechanics is referred to as the **collapse of the wave function**. It remains up to date one of the most poorly understood and more puzzling aspects of quantum theory.

# Dirac notation

The generalised statistical interpretation can also be formulated in terms of the **Dirac notation**, which is more convenient in some quantum systems such as for example the spin of the electron.

In this notation, if we have a generic state vector (element of the Hilbert space of the system) denoted by $|\Psi(t)\rangle$, we can always express it as a linear superposition of the eigenvalues of $\hat{Q}$, which we denote by $\hat{Q}|\psi_n\rangle =q_n|\psi_n\rangle$, as follows
$$
|\Psi(t)\rangle = \sum_l c_l(t) |\psi_l\rangle ~~{\rm (before)}
$$
where you can check that, using the orthonormality property $\langle \psi_n | \psi_l\rangle = \delta_{nl}$, the coefficients of this expansion are computed with the following bracket:
$$
c_n(t) = \langle \psi_n | \Psi(t) \rangle
$$
Then the probability of measuring $Q=q_n$ in the quantum state $|\Psi(t)\rangle$ will be given by
$$
P(q_n) = |c_n(t)|^2 = |\langle \psi_n | \Psi(t) \rangle|^2
$$
and after the measurement my quantum state will **collapse** to the eigenvector associated to $q_n$, namely
$$
|\Psi(t)\rangle = |\psi_n\rangle ~~{\rm (after)}
$$
which as mentioned before it is a determinate state of $Q$.

Now, if $P(q_n) = |c_n(t)|^2$ is the probability of measuring $Q=q_n$, what will be its sum over **all possible outcomes** of the measurement? 

It follows from the axioms of the generalised statistical interpretation that
$$
\sum_l P(q_l) = \sum_l |c_l(t)|^2 = 1
$$
since the probability that we measure any possible outcome is of course unity. 

Let us verify that this important property is satisfied. To do this, we will use the fact that the eigenvectors of an Hermitian operator are **complete**, a property that can be expressed as
$$
\sum_{l} |\psi_l \rangle \langle \psi_l| = \mathbb{1} \, ,
$$
where $\mathbb{1}$ is the unit operator.

Using this property, we can demonstrate that
$$
\sum_l |c_l(t)|^2 = \sum_l \langle \Psi(t) | \psi_l \rangle\langle \psi_l | \Psi(t) \rangle = \langle \Psi(t) | \Psi(t) \rangle =1
$$
as expected, where we have used that $| \Psi(t) \rangle$ is normalised and the completeness relation of the eigenvectors $\{q_n \}$.

So indeed we verify the expectation that the sum of the probablities of all possible outcomes adds up to one, as should always be the case with probabilities.

# Expectation values

Now that we have seen how quantum mechanics allows us to evaluate the probabilities associated to the outcomes of general measurements, we can use this information to evaluate the **expectation value** of the physical observable $Q$ in a compact manner.

From basic considerations in probability theory, one should expect that if the probability of measuring $q_n$ is $P_n=|c_n(t)|^2$, then one should have that expectation value of $Q$ is given by

$$\langle Q \rangle=\sum_n P_n q_n = \sum_n |c_n(t)|^2 q_n$$

Let us verify that this is indeed the case, starting from the quantum-mechanical definition of the expectation value of the observable $Q$ in the quantum state $\Psi(t)$:

> *Proof*:
>
> $$\langle Q \rangle=\langle \Psi(t) | \hat{Q} \Psi(t) \rangle = \Big\langle \sum_m c_m(t) \psi_m\Big| \hat{Q} \sum_n c_n(t) \psi_n \Big\rangle = 
> $$
> 
> $$ \sum_m \sum_n c_m^*(t)c_n(t) \langle \psi_m| \hat{Q}\psi_n \rangle = \sum_m \sum_n c_m^*(t)c_n(t) q_n \langle \psi_m| \psi_n \rangle =$$
> 
> $$
>     \sum_m \sum_n c_m^*(t)c_n(t) q_n \delta_{mn} =\sum_n |c_n(t)|^2 q_n
> $$
> 
> as we wanted to show.

Therefore, once we use quantum theory to evaluate the probabilities associated to the outcomes of various measurements, we can use the **standard statistics** to evluate estimators such as the mean value or the variance of the observable $Q$.

