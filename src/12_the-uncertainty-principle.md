---
title: The uncertainty principle
---

**Week 4, lecture 12**

!!! summary "Learning objectives"

    After following this lecture, you will be able to:

    - **Generalise Heisenberg's uncertainty** principle to an arbitrary pair of physical observables.
    - Derive the uncertainty relation associated to any pair of incompatible observables.
    - Reproduce **known results** for the Heisenberg's uncertainty principle applied to the position and linear momentum of a particle.
    - Understand the difference between **compatible** and **incompatible observables**.


# The uncertainty principle revisited

In this lecture we show that the Heisenberg's uncertainty principle that we discussed in previous lectures is a particular case of a more general principle, and determine the conditions whereby two physical observables will satisfy a Heisenberg-like uncertainty relation.

## Heisenberg's uncertainty principle

In previous lectures, you have already encountered the **Heisenberg's uncertainty principle** applied to the position $x$ and the linear momentum $p$ of a particle, which states that:

$$\Delta x \Delta p \geq \frac{\hbar}{2} \, $$

where $\Delta x$ ($\Delta p$) indicates the **uncertainty** associated to the measurement of the position (momentum) of a particle in a given quantum state.

Heisenberg's uncertainty principle tells us that in quantum mechanics it is not possible to **measure simultaneously** the position and the linear momentum of a particle, and that the best our knowledge of its position (the smallest the value of $\Delta x$) is, the worse our knowledge of its momentum (the larger the value of $\Delta p$) will be.

You have already found several examples that demonstrate the impact of the Heisenberg principle in the properties of quantum systems. For example, the free particle solutions with well-defined energy $E$ were characterized by $\Delta p=0$, and hence we should expect that the uncertainty in its position blows up, $\Delta x\to \infty$. Indeed, free-particle states with fixed energy $E$ were **maximally delocalised** and non-normalisable, thus satisfying the Heisenberg uncertainty relation.

Another similar relation that you have seen before is the Heisenberg's uncertainty principle relating **energy and time**,

$$\Delta E \Delta t \geq \frac{\hbar}{2}$$

which told you that very precise measurements of the energy of a particle ($\Delta E \to 0$) could only be carried out over a large time interval, $\Delta t \to \infty$.


In this lecture, we aim to determine under which conditions these kind of uncertainty relations will hold for two general observables, denoted by $A$ and $B$. As we will show, a Heisenberg-like principle will always arise for any pair of observables $A$ and $B$ for which the corresponding Hermitian operators $\hat{A}$ and $\hat{B}$ **do not commute**: $\hat{A}\hat{B}\ne \hat{B}\hat{A}$.

This result is often called  the **generalized uncertainty principle**, though actually as we will show it is not really a principle itself, but it is a** direct consequences **of the fundamental axioms of quantum mechanics. The previous uncertainty relations, such as $\Delta x \Delta p \ge \hbar/2$, will be shown to be particular cases of this more general principle.

## Derivation

To derive this generalized uncertainty principle, first of all we need to evaluate the **variance**  $\sigma^2_A$ of a general operator $\hat{A}$. In statistics, the variance (or more accurately, the **standard deviation** $\sigma_A$) is a measure of the uncertainty (lack of knowledge) associated to the stochastic variable $A$.

The variance $\sigma_A^2$ of the observable $A$ in the quantum state $|\psi \rangle$ (we will use Dirac notation through this lecture) is defined as:

$$\sigma_A^2=\langle A^2\rangle-\langle A\rangle^2= \langle (\hat{A}-\langle A\rangle )^2\rangle$$

that is, as the expectation value of the square of $\hat{A}-\langle A\rangle $, the operator $\hat{A}$ with its mean value subtracted.

>*Proof*:
    $\langle (A-\langle A \rangle)^2\rangle = \langle A^2+ \langle A\rangle^2-2A \langle A\rangle \rangle= \langle A^2\rangle+ \langle A\rangle^2-2 \langle A \rangle \langle A \rangle= \langle A^2 \rangle- \langle A\rangle^2$

We can also express the variance of the observable $A$ in the following manner:

$$\sigma_A^2 = \langle (\hat{A}- \langle A \rangle )^2 \rangle= \langle  \psi  | (\hat{A}- \langle A \rangle )^2 \psi\rangle= \langle ( \hat{A} - \langle A \rangle) \psi | ( \hat{A} - \langle A \rangle) \psi \rangle = \langle f| f \rangle$$

where we have defined 
$$
|f\rangle \equiv ( \hat{A} - \langle A\rangle) |\psi \rangle
$$
which is the state vector that one gets when acting with the operator $( \hat{A} - \langle A\rangle)$ on the ket $|\psi \rangle$. Furthermore, we have used the the operator $\hat{A}$ is Hermitian, and therefore it can be freely moved from the ket to the bra and viceversa.

Likewise, for a different physical observable $B$ its variance will be given by,

$$\sigma_B^2=\langle g | g \rangle$$

where now we have defined the quantum state $|g\rangle \equiv ( \hat{B} - \langle B \rangle)| \psi \rangle$.

Now we are ready to derive the generalized uncertainty principle. For this, in analogy with the usual Heisenberg uncertainty relations, we need to compute what is the product of the variances (uncertainties) associated to the operators $\hat{A}$ and $\hat{B}$, that is, we have to evaluate

$$\sigma_A^2 \sigma_B^2=\langle f | f \rangle \langle g | g \rangle \, .$$ 

For this, we can apply the **Schwarz inequality**.

!!! information 
    **The Schwarz inequality**: for any two vectors $\vec{u}$ and $\vec{v}$, part of a vector space equipped with a inner product, then the following inequality holds:

    $$|\vec{u}\cdot \vec{v}|^2 \leq \left( \vec{u}\cdot\vec{u}\right) \left( \vec{v}\cdot\vec{v}\right) = |u|^2|v|^2$$

    where $|u|$ and $|v|$ indicate the magnitude (norms) of the vectors $\vec{u}$ and $\vec{v}$. 
    The  Schwarz inequality can also be expressed in the Dirac notation relevant for Hilbert spaces, by recalling that the scalar product is here the braket between two state vectors and hence:

    $$|\langle u | v \rangle|^2 \leq  \langle u | u \rangle \langle v | v \rangle$$

Since the Hilbert space is also a vector space, the Schwarz inequality is guaranteed to hold there and hence we can express the product of the variances of $A$ and $B$ as:

$$\sigma_A^2 \sigma_B^2=\langle f | f \rangle \langle g | g \rangle \geq |\langle f | g \rangle|^2$$ 

Now to make progress we need to do some algebra and review a useful property of complex numbers:
    
!!! information
    For any complex number $z$, we have that its norm squared $|z|^2$ will be bigger than the square of its imaginary component, given that

    $$|z|^2=[\Re (z)]^2+[\Im (z)]^2 \geq [\Im (z)]^2$$

    and hence since $[\Im (z)]^2=[\frac{1}{2i} (z-z^*)]^2$ we will have that

    $$ |z|^2 \geq \left(\frac{1}{2i} (z-z^*)\right)^2$$

Therefore, letting $z=\langle f | g \rangle$ we can apply this general property of complex numbers to our derivation

$$\sigma_A^2 \sigma_B^2 \geq |\langle f | g \rangle|^2$$

$$|\langle f | g \rangle|^2=\left( \frac{1}{2i}\left(\langle f | g \rangle - \langle g | f \rangle\right)\right)^2$$

therefore 

$$\sigma_A^2 \sigma_B^2 \geq \left(\frac{1}{2i} \left( \langle f | g \rangle - \langle g | f \rangle\right)\right)^2$$

Now, the final step of our derivation is to evaluate the inner product $\langle f | g \rangle$ and its complex conjugate $\langle g | f \rangle$. After doing some algebra and exploiting again that $\hat{A}$ and $\hat{B}$ are Hermitian operators (and hence can be moved from bras to kets), one finds

$$\langle f | g \rangle=\langle \hat{A} \hat{B} \rangle - \langle A \rangle \langle B \rangle$$

$$\langle g | f \rangle=\langle \hat{B} \hat{A} \rangle - \langle A \rangle \langle B \rangle$$

and therefore we obtain our final result for the product of the variances of the observables $A$ and $B$:

$$\sigma_A^2 \sigma_B^2 \geq \left(\frac{1}{2i} \left(\langle\hat{A} \hat{B} \rangle -  \langle\hat{B} \hat{A} \rangle \right)\right)^2$$

This result can be expressed in a more compact manner if we define the **commutator** of two operators as

$$
\left[ \hat{A},\hat{B}\right]\equiv \hat{A}\hat{B}-\hat{B}\hat{A} \, .
$$

The commutator between two operators is an operator itself. If two operators **commute**, then it does not matter in which order do we apply them to a general quantum state and their commutator will vanish.

!!! warning
    In quantum mechanics we always should be careful with the **order** in which we apply two operators $\hat{A}$ and $\hat{B}$, since the result of applying their product in some quantum state, $\hat{A}\hat{B}|\psi\rangle$ will be different if the order is reversed, $\hat{B}\hat{A}|\psi\rangle$. Recall that this property also holds for matrices, so one cannot freely change their order in an expression as one does with scalars.

If we express the **generalised uncertainty principle** in terms of the commutator of $\hat{A}$ and $\hat{B}$, we obtain our final result.

$$\sigma_A^2 \sigma_B^2 \geq \left( \frac{1}{2i}\langle [\hat{A}, \hat{B}] \rangle\right)^2$$

or its equivalent relation in terms of the standard deviation rather than the variances

$$
\sigma_A \sigma_B \geq \Bigg| \frac{1}{2i}\langle [\hat{A}, \hat{B}] \rangle\Bigg|
$$

where note that both the variances and the standard deviation are positive-definite quantities, and thus the RHS of this equation is guaranteed to be a real, positive number. 

Let us discuss some important consequences of the generalized uncertainty principle:

1. If two physical observables commute, that is, if their associated Hermitian operators satisfy $[\hat{A},\hat{B}]=0$, then $\sigma_A^2 \sigma_B^2 \geq 0$.    

    This implies that we **can measure both observables simultaneously** with arbitrarialy good precision. In this case we say that $A$ and $B$ are **compatible observables**.
    
    Compatible observables have the important property that they admit a common set of eigenvectors. That is, if $\hat{A}$ and $\hat{B}$ commute, then we know that there exist a complete basis of eigenvectors $|\psi_n\rangle$ which satisfies both $\hat{A}|\psi_n\rangle = q_{A,n}|\psi_n\rangle$ and $\hat{B}|\psi_n\rangle = q_{B,n}|\psi_n\rangle$.


2. If two physical observables do not commute, that is, if their commutator does not vanish $[\hat{A},\hat{B}] \neq 0$, then we say that these two observables are **incompatible**. 
    
    In this case, $A$ and $B$ will obey a Heisenberg-like uncertainty relation as demonstrated above and one cannot measure them simultaneously with arbitrary precision. 

    Furthermore, there does not exist a common complete basis of eigenvectors that satisfy the two eigenvalue equations associated to $\hat{A}$ and $\hat{B}$.

It is now easy to verify that the Heisenberg uncertainty relation for $x$ and $p$ was just a particular case of this generalised uncertainty principle that we have just derived.

To show this, start from the general expression

$$
\sigma_A \sigma_B \geq \Bigg| \frac{1}{2i}\langle [\hat{A}, \hat{B}] \rangle\Bigg|
$$

and now apply it to $A=x$ and $B=p$, so we have

$$
\sigma_x \sigma_p \geq \Bigg| \frac{1}{2i}\langle [\hat{x}, \hat{p}] \rangle\Bigg|
$$

Now, since we know what the commutator of $x$ and $p$ gives, $[\hat{x}, \hat{p}]=i\hbar$, we end up finding

$$
\sigma_x \sigma_p \geq \frac{\hbar}{2}
$$

which is as expected nothing but the original uncertainty principle.

## The free particle revisited

It is now instructive to connect this discussion of the generalised uncertainty principle with what we know about the free particle quantum system. 

For a free particle, the eigenfunctions of the Hamiltonian were given by $\psi_k(x)=Ae^{\pm ikx}$, where $k\equiv \sqrt{\frac{2mE}{\hbar^2}}$.

What does the uncertainty principle tell us in the case of a free particle?

Let us apply the linear momentum operator to this wavefunction

$$\hat{p} \psi_k(x)=\frac{\hbar}{i}\frac{d \psi_k(x)}{d x}=\pm \frac{\hbar}{i} i k A e^{ikx}=\pm \hbar k \psi_k(x)$$

which tells us that $\psi_k(x)$ is a **determinate state ** of the linear momentum $p$: we always find the same result if we measure its linear momentum in the state $\psi_k(x)$.

!!! information
    You can convince yourselves that the variance of an observable $A$ in one of its determinate states $|\psi\rangle$ (such that $\hat{Q}|\psi\rangle=a|\psi\rangle$) is always zero, since clearly $\langle A \rangle = a$ for this determinate state and thus $|f\rangle = ( \hat{A} - \langle A\rangle) |\psi \rangle=0$, implying that $\sigma_A=0$.

Therefore, for an eigenfunction of free particle Hamiltonian,  $\sigma_p =0$ and the Heisenberg uncertainty principle tells us that the variance of the position will be $\sigma_x \geq \frac{\hbar /2}{\Delta p} \rightarrow \infty$: we have a maximal uncertainty about the position of the particle.

Now, when discussing the free particle we showed how one can construct [wave packets](https://qm1.quantumtinkerer.tudelft.nl/6_the_free_particle/) that lead to normalisable wave functions by means of combining waves with a range of different energies.

If we construct a Gaussian wave packet using the profile function $\psi_k(x)=Ae^{-ax^2}$, then it can be shown that for this configuration the generalised uncertainty principle leads to 

$$
\sigma_x \sigma_p =\frac{\hbar}{2} \, .
$$

Therefore, this means that a Gaussian wave packet has the **minimum possible uncertainty**: if we want to construct a free particle state for which we can determine both its position and its momentum simultaneously with the highest possible accuracy, then a Gaussian wave packet is guaranteed to produce the best results (smallest uncertainty in both $x$ and $p$).







