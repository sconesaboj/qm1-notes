---
title: The Formalism of Quantum Mechanics
---

**Week 3 & 4, lecture 9 & 10**

!!! summary "Learning objectives"

    After following this lecture, you will be able to:
    
    - Describe quantum states in terms of the Dirac notation.
    - Apply the language of linear algebra to describe the basic operations of quantum mechanics.
    - Stablish the correspondence between physical observables and operators acting on quantum states.
    - Determine which conditions a linear operator must satisfy to represent a physical observable and what are the implications of these conditions.

# Observables and quantum states

In the previous lectures, we have discussed the application of quantum mechanics to the description of various important systems, from the harmonic oscillator to the finite square well and barrier potentials. We have presented the various solutions that the Schroedinger equation admits for these systems, and their implications both for scattering and for bound states.

Now we are going to take a step back and present a more abstract picture of quantum theory. We will discuss the **general formalism** of quantum mechanics and show how the relevant mathematical language is that of **linear algebra**, both to describe a general quantum state as well as the operations that can be carried out on them. We will show how the concept of **measurement** is dramatically different as compared to classical physics, and provide a prescription to predict the outcome of measurements of specific observables in a general quantum state.

## Measurements in quantum mechanics

Any physical experiment is composed of two steps: **preparation** and **measurement**. The specific preparation of the experimental set-up determines the possible outcomes of the experiment, while the subsequent measurements determine the actual value of these outcomes. For example, I can prepare my system in a given way, and then depending on the measurement apparatus that I chose I will be able to measure one or another of its properties.

This relation between preparation and measurement is rather different in classicial and quantum physics, as illustrated by the following two examples:

!!! example "Measurements in classical mechanics"
    Assume that we want to measure the acceleration of gravity by using a pendulum of length $L$. What we should do is to measure the period $T$ and then use classical mechanics to evaluate $g=4\pi^2 L/T^2$ using the predictions from Newton's theory. We can verify for example that the results are always the same if I change the value of the pendulum mass. If my measurement apparatus is perfect, I will obtain in all cases the same exact result if I prepare my system in the same way. In other words, provided the pendulum's length $L$ is kept fixed, I will always obtain the same value of $g$ every time I measure it in such system.

Hence in classical physics, neglecting instrumental limitations, the results of any measurement can be univocally predicted and the outcome of the same measurement on identical systems will always be the same. What happens in quantum theory?

!!! example "Measurements in quantum mechanics"
    Assume now that one has a quantum system that can be found in two states, $\psi_1$ and $\psi_2$. For example, $\psi_1$ and $\psi_2$ could be two of the solutions of the Schr\"odinger equation for the quantum harmonic oscillator. Now one prepares identical copies of this system with the same combination of the two states, $\psi_1$ and $\psi_2$, say $\Psi= a \psi_1+b\psi_2$. As opposed to classical physics, now every time one carries out a measurement in these identical quantum systems we will find a **different result** (even if my measurement apparatus is perfect). 

In other words, quantum mechanics does not allow one to make predictions about **the outcome of individual measurements**, only about the statistical properties of a large number of measurements of the same quantum system. As we will show below, in the case of a quantum state $\Psi= a \psi_1+b\psi_2$ where $\psi_1$ and $\psi_2$ are two solutions of the SE with energies $E_1$ and $E_2$, what quantum theory **does predict** are the probabilities $P(E_1)=|a|^2$ and $P(E_2)=|b|^2$.

These two steps, first the preparation of the experiment, and second the measurement of some specific property of the system, can be represented in quantum mechanics by what are called the **state of the system** and the **physical observable** respectively. In other words, the **state of the system** provides a complete description of the set of probabilities associated to all conceivable measurements, while the set of **observables** associated to a quantum system are all the dynamical variables that can be measured, such as energy, position, or angular momentum. Given the state of a quantum system, we can predict the **probabilities** associated to the outcomes of measuring different observables.

In this lecture we present which are the mathematical quantities which in quantum theory describe the **state of a system** and the **physical observables**, and what are the mathematical relations that these must obey. Given that the relevant mathematical language is that of **linear algebra**, you can check [these lecture notes](https://mathforquantum.quantumtinkerer.tudelft.nl/) to refresh the relevant basic concepts.

## The quantum state

Up to now, we have emphasized that the state of a quantum system is completely characterized by its wavefunction, $\Psi(x)$. We would like now to talk about **quantum states** in a rather more abstract way, as elements of a special **vector space** governed by the familiar rules of linear algebra. 

!!! recall
    You are already familiar with vector spaces in Cartesian coordinates. For example, in three dimensions ($\mathbb{R}^3$) we can express any vector $\vec{v}$ as a linear superposition of three basis vectors, $\vec{v}=a_1 \vec{v}_1+a_2 \vec{v}_2 + a_3 \vec{v}_3$ where $\vec{v}_1, \vec{v}_2, \vec{v}_3$ are orthonormal ($\vec{v}_i \cdot \vec{v}_j = \delta_{ij}$). You can remind ourselves of the basic concepts of vector spaces [here](https://mathforquantum.quantumtinkerer.tudelft.nl/3_vector_spaces/).

As we will show, this language reveals the full complexity of the **formalism of quantum mechanics** and makes possible achieving a deeper understanding of the implications of quantum theory.

In this more abstract notation, we will denote a quantum state with the $|\psi\rangle$ symbol, which is known as a **ket**. A ket is an element of an abstract complex vector space called the **Hilbert space** $\mathcal{H}$. For this reason we will also denote a quantum state by **state vector**.

!!! info "Dimensionality of the Hilbert space"
    In the regular vector spaces that you are familiar with, their dimensions are defined by the number of orthogonal basis vectors that are required to span the space, that it, to represent any arbitrary member of this space via a linear combination. For the same reason, the dimension of a Hilbert vector space $\mathcal{H}$ is defined by the **minimal number of independent state vectors** that are required to represent an arbitrary member of this space via a linear combination.

The concept of the dimensionality of the Hilbert space is subtle, so let us illustrate it with two examples, corresponding respectively to Hilbert spaces of finite and infinite dimensions.

!!! example
    The electron spin can only be found in two states, which we can denote as _pointing up_ ($|\uparrow\rangle$) and _pointing down _($|\downarrow\rangle$). This means that the **most general element** of this Hilbert space can be expressed by the linear combination of this two states:
    
    $$|\psi\rangle=a|\uparrow \rangle + b|\downarrow \rangle$$
    
    where $a$ and $b$ are arbitrary complex numbers subject to the normalisation condition. Since with only two state vectors we span the whole space (that is, its basis contains two elements) we conclude that the electron spin has associated a Hilbert space of **dimension 2**.

Dimension 2 is the lowest dimension that a vector space can have. Quantum systems can be described by Hilbert spaces with dimensionality starting from two and all the way to infinity, as shown by the next example.

!!! example
    A free particle can be found anywhere in space $x$. If I denote the basis kets as $|x \rangle$, then to describe a general quantum state I will need an infinite number of them, since **all positions** are allowed. In this case, the most general state $|\psi\rangle$ is written as
    
    $$ |\psi\rangle = \int_{-\infty}^\infty dx \,\psi(x) |x \rangle$$
    
    where $ \psi(x)$ is the standard wave function. Since I need an infinite number of basis vectors, we conclude that the free particle has associated a Hilbert space of **infinite dimensionality**. Note that in such cases the linear superposition is expressed in terms of an **integral** rather than a sum.

Furthermore, one should be careful since the dimensionality of the Hilbert space is in general not the same as the _physical (spatial) dimensions_ of the system under consideration.

!!! warning
    You should not confuse the dimensionality of the Hilbert space with that of the spatial dimensions of your quantum system. For example, the free particle moves in a _one dimensional physical space_ but its state vectors belong to an _infinite dimensional_ Hilbert space. 
  
In this Hilbert space, the **physical observables** $Q$ will be represented by **linear operators** that we will denote as $\hat{Q}$ and that correspond to linear transformations acting on the state vectors and mapping them into other elements of the vector space, $\hat{Q}|\psi_1\rangle=|\psi_2\rangle$ where in general $|\psi_1\rangle \ne |\psi_2\rangle$, and both state vectors belong to the same Hilbert space, $|\psi_1\rangle,|\psi_2\rangle \in \mathcal{H}$.

This implies that we will be able to represent physical actions applied to a quantum system (such as the time evolution or the measurement of some observable like the energy or the position) in terms of linear operators acting on the state vectors of the system.

Having the wavefunctions belonging to a vector space and with physical observables represented by operators (linear transformations) in this space, it is clear that the mathematical language relevant to describe quantum mechanics will be **linear algebra**. 

!!! summary "In summary"
    The **formalism of quantum mechanics** is built upon two fundamental concepts:

    - The state of a quantum system is completely specified by its **state vector** $|\Psi \rangle$, which is an element of an abstract complex vector space known as the **Hilbert space** $\mathscr{H}$, $|\Psi \rangle \in \mathscr{H}$. All physical information about a given quantum state is encapsulated in its state vector $|\Psi \rangle$.
    
    - These state vectors are modified by **linear operators** that act upon them (and transform them into other elements of the same Hilbert space) and that determine the outcome of measurements on the system.

Since in quantum mechanics state vectors are members of the Hilbert vector space, operations among them will follow the standard rules of linear algebra.  For instance, if we have two state vectors belonging to the same Hilbert space, $|\phi\rangle, |\psi\rangle \in \mathscr{H} $, the sum of the two state vectors $|\phi\rangle + |\psi\rangle = |\rho\rangle$ must also be an element of the same Hilbert space, $|\rho\rangle \in \mathscr{H} $. 

Likewise, linear algebra tells us that the following relations should hold between elements of a Hilbert space (as they hold for any vector space):

- *Commutative property*: $|\phi\rangle + |\psi\rangle = |\psi\rangle + |\phi\rangle$
- *Associative property*: $|\phi\rangle + (|\psi \rangle + |\rho \rangle) = (|\phi\rangle + (|\psi \rangle ) + |\rho \rangle$
- *Multiplication by a scalar*: the product of a vector $|\phi\rangle$ with a complex quantity $c\in \mathbb{C}$ results in another Hilbert space vector: $c|\phi\rangle \equiv |c\phi\rangle$
- *Distributive property*: $c( |\phi \rangle + |\psi \rangle)= c|\phi\rangle + c|\psi \rangle; ~~c\in \mathbb{C}$

Another operation that you are familiar with in the case of vector spaces is the **scalar product** between two vectors, $\vec{v}_1 \cdot \vec{v}_2=|\vec{v}_1||\vec{v}_2|\cos \theta$ where $\theta$ is the angle between the two vectors. The scalar product maps two vectors into a real number, and it vanishes for two orthogonal vectors. As we will show below, also in Hilbert spaces we will have a scalar product between two state vectors, which will be called a **braket**. 

### Representations of state vectors

In the same way as a vector can be expressed in terms of different bases and coordinate systems (say Cartesian or spherical coordinates), also a state vector (ket) $|\Phi\rangle$ admits different possible representations. The available representations depends both on the dimensionality of the Hilbert space (finite or infinite) as well as on the choice of basis. Recall from linear algebra that the vector itself is unchanged when expressed in a different basis, but its _coordinates_ will vary.

!!! example

    Let's take a vector in a 2-dimensional real space,

    $$\vec{v}=(2,1)$$

    If the basis is $\vec{i}=(1,0)$ and $\vec{j}=(0,1)$ then we can express this vector as a function of its bases elements: $\vec{v}=2\vec{i}+\vec{j}$

    But now if the bases vectors are $\vec{i'}=(\frac{1}{\sqrt{2}},\frac{1}{\sqrt{2}})$ and $\vec{j'}=(\frac{1}{\sqrt{2}},-\frac{1}{\sqrt{2}})$ you can show that the same vector $\vec{v}$ now can be express as, 

    $$\vec{v}=\frac{3}{\sqrt{2}}\vec{i'}+\frac{1}{\sqrt{2}}\vec{j'}$$

    So, we can express the same vector in terms of two different bases.

The same is true for quantum states, and they can be expressed in terms of different basis, which we call **representations**.

Up to now we have been dealing mostly with Hilbert spaces of infinite dimensions. For example, a particle moving in one dimension can be found in any possible position $x$. And we said that its state is fully specified by the wavefunction $\psi(x)$. What we have been implicitely using is the so-called **position space representation**, where the ket $|\Psi\rangle$ is expressed in the basis of position eigenstates $|x\rangle$

$$ |\Psi\rangle = \int_{-\infty}^\infty dx \,\psi(x) |x \rangle$$ 

and since the dimensions of the Hilbert space are infinite, a linear superposition of basis elements is actually an integral.

But we could have also used the **position space representation**, where the _same state vector_ is now expressed in terms of a basis of states with well-defined linear momentum:

$$ |\Psi\rangle = \int_{-\infty}^\infty dp \,\phi(p) |p \rangle$$ 

where $\phi(p) $ is the momentum space wavefunction. 

In the same way as how in a regular vector space we can transform our vectors between different basis, also for a Hilbert space we can transform the wave function from one representation to the other. In this case, the  momentum and position space wavefunctions are related by a Fourier transform:

$$
\phi(p) = \frac{1}{\sqrt{2 \pi \hbar}}\int_{-\infty}^\infty  dx~\psi(x)e^{-ipx/\hbar} \, .
$$

In the case of **finite Hilbert spaces**, such as the electron spin discussed above, it is more convenient to adopt a **matrix representation** where a state vector is represented by a _column vector_ with entries the coefficients of the linear expansion of $|\Psi\rangle$ in a given basis. 

For example, assume we have an $n$-dimensional Hilbert space. In this space, we have a basis composed by $n$ vectors $\{ |\psi_i \rangle \}$. This means that we can express our state vector $|\Psi\rangle$ in terms of the basis vectors as follows
$$
|\Psi\rangle = \sum_{i=1}^n c_i |\psi_i \rangle \, .
$$
with $c_i$ being general complex coefficients. In this matrix representation, we can express $|\Psi\rangle$ as the following column vector

$$ |\Psi\rangle =\begin{pmatrix}c_1\\c_2\\c_3\\ \vdots \\c_n\end{pmatrix}$$

Of course, if I change the basis vectors the coefficients of this column vector will be different. As we will see in this course, a clever choice of basis can significantly facilitate the solutions of many problems in quantum mechanics.

The basis vectors $\{ |\psi_i \rangle \}$, in the same way as in a regular vector space, should be orthonormal among them. To quantify what we mean by this, we need to define an inner product between elements of a Hilbert space, but first we need to introduce the important concept of _dual Hilbert space._

### The dual Hilbert space

The dual Hilbert space $\mathcal{H}^*$ is another vector space, closely related to the Hilbert space but not identical to it. Elements of this dual Hilbert space are denoted as **bras** and we use the notation $\langle \Psi|$ for these elements.

There exists a **one-to-one correspondence** between elements of the original Hilbert space $\mathcal{H}$ and of its dual $\mathcal{H}^*$. Given a ket $|\Psi\rangle$ its corresponding bra $\langle \Psi|$ will be given by:

- Position space representation:
  
  $$ |\Psi\rangle = \int_{-\infty}^\infty dx \,\psi(x) |x \rangle \quad \to \quad \langle \Psi| = \int_{-\infty}^\infty dx \,\psi^*(x) \langle x |$$

  That is, we take the _complex conjugate_ of the position space wave function.

- Momentum space representation:
  
  $$ |\Psi\rangle = \int_{-\infty}^\infty dp \,\phi(p) |p \rangle \quad \to \quad \langle \Psi| = \int_{-\infty}^\infty dp \,\phi^*(p) \langle p |$$

  That is, we take the _complex conjugate_ of the momentum space wave function.

- Matrix representation
  
  $$ |\Psi\rangle =\begin{pmatrix}c_1\\c_2\\c_3\\ \vdots \\c_n\end{pmatrix}      \quad \to \quad  \langle \Psi| =\begin{pmatrix}c^*_1 & c^*_2 & c^*_3 & ... & c^*_n\end{pmatrix}  $$

  So we transform the original column vector into a _row vector_ and take the _complex conjugate of its coefficients_.

The notation that we will use to express how a **bra** is obtained in terms of the corresponding **ket** will be:

$$\langle a|=(|a\rangle)^\dagger$$

Armed with the concept of dual Hilbert space, we can now define what is the inner product between two elements of $\mathcal{H}$.

!!! info
    The notation whereby state vectors in a Hilbert space (and their duals) are represent as **kets** $|\alpha\rangle$ and **bras** $\langle \beta |$ while their inner product is represented by a **braket** $\langle \beta|\alpha \rangle$ is known as the **Dirac notation**, in recognition of Paul Dirac who first introduced it when developing the foundations of quantum mechanics.


### Inner product 

From your study of linear algebra, you are also familiar with the concept of _scalar_ or _inner_ product between two vectors, where one can multiply two vectors together to obtain a scalar (just a number). 

!!! recall
    For example, in Euclidean space $\mathbb{R}^n$ we have the _scalar product_ between two vectors $\vec{a}$ and $\vec{b}$ defined as $\vec{a} \cdot \vec{b} = \sum_{i=1}^n a_ib_i \, \in \mathbb{R}$ in terms of their components. You can also express this result as $\vec{a} \cdot \vec{b} =  |\vec{a} ||\vec{b} |\cos\theta$ in terms of the magnitude of each vector and its relative angle $\theta$.

Likewise, in the case of the Hilbert spaces relevant to describe quantum systems, we can also define an **inner product** between two quantum states. For two state vectors $|a\rangle$ and $|b\rangle$, we denote their inner product as $\langle a|b \rangle \in \mathbb{C}$ in what is called a **braket** in the Dirac notation (explaining the choice of _bra_ and _ket_ terms for the elements of a Hilbert space). 

Note that since the Hilbert space is a complex vector space, the inner product between two state vectors will be in general a complex number. Here $\langle a|$ is the **bra**, element of the _dual Hilbert space_ $\mathscr{H}^*$, corresponding to the **ket** $|a\rangle$ and constructed using the above prescriptions. 

In the position-space representation that we have been mostly dealing with up to now, two state vectors $|a\rangle$ and $|b\rangle$ will have associated wave functions $\psi_a(x)$ and $\psi_b(x)$. In terms of these wave functions, the inner product between the state vectors $|a\rangle$ and $|b\rangle$ is defined as

$$\langle a| b \rangle\equiv \int_{-\infty}^{+\infty} \psi^*_a(x) \psi_b(x)\, dx;~~{\rm Eq.}~[1]$$ 

which as mentioned above it is a complex scalar. 

However, this is _not_ the only way in which the inner product between two state vector can be evaluated. In general, the expression of the inner product depends on the representation that is being used to describe our quantum system.

The specific way in which the inner product of two quantum states is computed depends on the specific representation:

- Position space representation: 

  $$
  \langle a|b \rangle \equiv \int_{-\infty}^{+\infty} \psi^*_a(x) \psi_b(x) dx
  $$

- Momentum space representation: 

  $$
  \langle a|b \rangle \equiv \int_{-\infty}^{+\infty} \phi^*_a(p) \phi_b(p) dp
  $$

- Eigenstate representation:

$$
\langle a|b \rangle = \begin{pmatrix}a^*_1 & a^*_2 & a^*_3 & \ldots & a^*_n \end{pmatrix}\begin{pmatrix}b_1 \\ b_2 \\ b_3 \\ \vdots \\ b_n \end{pmatrix}
$$

An important property of the inner product of two quantum states $|a\rangle$ and $|b\rangle$ is that the result is the same when computed in different representations or bases (since it is a scalar quantity).

!!! Example
    Let' us demonstrate how the result of the inner product between two state vectors is the same in the **position** and in the **matrix** representations:

    - Position space representation:

    $$\Psi_a(x)=\sum_n c_{na} \psi_n(x);~~\Psi_b(x)=\sum_m c_{mb} \psi_m(x)$$

    $$\langle a|b \rangle=\int_{-\infty}^{\infty}\Psi^*_a(x)\Psi_b(x)dx=$$

    $$=\int_{-\infty}^{\infty}\sum_n \left(c^*_{na} \psi^*_n(x)\right)\left(\sum_m c_{mb} \psi_m(x)\right)dx=$$

    $$=\sum_n \sum_m c^*_{na} c_{mb}\int_{-\infty}^{\infty}\psi^*_n(x)\psi_m(x)dx=\sum_n c^*_{na}c_{nb}$$

    where we have used the _linearity_ of the inner product to move the sums and the coefficients outside the integral, and also exploited that the basis elements of a vector space must be orthonormal under their inner product:
    $\int_{-\infty}^{\infty} \psi^*_n(x) \psi_m(x)dx= \delta_{nm}$

    - Matrix representation:

    $$|a\rangle = \begin{pmatrix}c_{1a}, c_{2a}, c_{3a}, ... , c_{na}\end{pmatrix}$$

    $$|b\rangle = \begin{pmatrix}c_{1b} \\ c_{2b} \\ c_{3b} \\ \vdots \\ c_{nb} \end{pmatrix}$$
    
    $$\langle a|b \rangle = \begin{pmatrix}c^*_{1a} & c^*_{2a} & c^*_{3a} & ... & c^*_{na}\end{pmatrix} \begin{pmatrix}c_{1b}\\c_{2b}\\c_{3b}\\ \vdots \\c_{nb}\end{pmatrix}=$$

    $$=\sum_n c^*_{na}c_{nb}$$

    where we have used the usual rules of vector multiplication.

    Hence we obtain the same result for the inner product irrespective whether we use the position-space representation or the matrix representation of our state vectors.

Recall that the inner product returns a complex scalar. From Eq. [1], we see that it satisfies $\langle a|b \rangle^*=\langle b|a \rangle$.

> **Proof:**
> 
> $$\langle a|b \rangle=\int_{-\infty}^{\infty}\Psi^*_a(x)\Psi_b(x)~dx$$
> 
> $$(\langle a|b \rangle)^*=\left(\int_{-\infty}^{\infty}\Psi^*_a(x)\Psi_b(x)dx\right)^*=$$
> 
> $$=\int_{-\infty}^{\infty}\Psi_a(x)\Psi^*_b(x)dx=\langle b|a \rangle$$

This result implies the inner product of a quantum state $|\Psi\rangle \equiv |a \rangle$ with itself is a positive real number:
    
> **Proof:**
> 
> $$\langle a|a \rangle=\int_{-\infty}^{\infty}\Psi^*_a(x)\Psi_a(x)dx=\int_{-\infty}^{\infty}|\Psi_a(x)|^2\,dx \in \mathbb{R}, \quad \ge 0$$

as could also be noted by the fact that if $\langle a|a \rangle^*=\langle a|a \rangle$ then $\langle a|a \rangle$ must be a real number.

As we saw in a previous lecture, the physical interpretation of the wavefunctions requires a normalized wavefunction, that is

$$\int_{-\infty}^{\infty}\Psi^*_a(x)\Psi_a(x)dx=1$$

Hence the formalism of quantum mechanics indicates that we can use the **inner product** to normalize quantum states. For instance if $|a\rangle$ is a quantum state not normalized, the corresponding normalized state $|\tilde a\rangle$ can be constructed as

$$|\tilde a\rangle = \frac{1}{\sqrt{\langle a|a\rangle}}|a\rangle$$

> **Proof:**
>
> $$\langle \tilde a|\tilde a\rangle = \left(\frac{1}{\sqrt{\langle a|a\rangle}}\right)^2\langle a|a\rangle=1$$

The inner product is a central ingredient to construct the **generalised probabilitic interpretation** of quantum mechanics, as we will show in a future lecture. 

After this discussion of the quantum states $|\Psi\rangle$ and their representations, we can discuss how to **act upon them** and transform one state vector into another by means of **linear operators**. 

## Operators

In quantum mechanics, for each possible physical observable $Q$ we will have associated a corresponding **linear operator** $\hat{Q}$. 

This correspondence between observables and operators is something that you have seen already for some important cases, for example

- $Q = p_{x}$ $\rightarrow$ $\hat{Q}=-i\hbar\frac{d}{dx}$ (**the momentum operator**)
- $Q = x$ $\rightarrow$ $\hat{Q}=x$ (**the position operator**)
- $Q = T$ $\rightarrow$ $\hat{Q}=-\frac{\hbar^2}{2m}\frac{d^2}{d x^2}$ (**the kinetic energy operator**)
- $Q = E$ $\rightarrow$ $\hat{Q}=-\frac{\hbar^2}{2m}\frac{d^2}{d x^2}+V(x)$ (**the energy operator**)

In previous lectures, we have seen that to determine the allowed wave functions and energies for a given quantum system we need to solve the corresponding Schroedinger equation

$$
\left( -\frac{\hbar^2}{2m}\frac{d^2}{d x^2}+V(x) \right)\psi_n(x) = \hat{H}\psi_n(x) = E\psi_n(x)
$$

where $n$ is the **quantum number** that labels the allowed energy states and the corresponding wave functions, $\hat{H}$ is the Hamiltonian operator associated to the energy $E$, and $V(x)$ is the potential experienced by the particles in our system.

By expressing the SE in this form,
$$
\hat{H}\psi_n(x) = E\psi_n(x)
$$
and recalling that the wavefunctions $\psi_n(x)$ are elements of a vector space (the Hilbert space), we see that it has the same form as an [eigenvalue equation in linear algebra](https://mathforquantum.quantumtinkerer.tudelft.nl/6_eigenvectors_QM/). 

In other words, solving a quantum system and determining the allowed wave functions and energies requires _solving an eigenvalue equation_. 

It turns out that this is also true for **other observables** beyond the energy $E$: if I want to know the possible outcomes of measuring the observable $Q$, I will need to solve the eigenvalue equation associated to its operator $\hat{Q}$. 

!!! info
    The *eigenvalue equation* associated to an operator $\hat{Q}$ living in the Hilbert space $\mathcal{H}$ is given by:

    $$\hat{Q}|\psi_n\rangle=\lambda_n |\psi_n\rangle$$

    which in general has many solutions. The kets $|\psi_n\rangle$ are called the **eigenstates**, **eigenfunctions**, or **eigenvectors** of $\hat{Q}$. Each solution $\psi_n\rangle$ has associated a complex scalar $\lambda_n$ called the **eigenvalue** corresponding to* the eigenvector $|\psi_n\rangle$. Note that in general eigenvalue equations can exhibit _degeneracy_, implying that two or more different eigenvectors can have the same eigenvalue.

Hence, the formalism of quantum mechanics tell us that for each observable $Q$, there is an associated specific eigenvalue equation to be solved. For example, in the position representation, an operator $\hat{Q}$ living in a Hilbert space $\mathscr{H}$ with eigenvectors $\psi_n(x)$ fullfils the eigenvalue equation:

$$\hat{Q}\psi_n(x)=q_n\psi_n(x);~~{\rm Eq.}~[2]$$

where being $q_n$ being its corresponding eigenvalues.

If there exist several different eigenvectors to the same eigenvalue, then the eigenvalue is called **degenerate**.

Solving Eq. [2] for the operator $\hat{Q}$ representating the observable $Q$ is crucial: the resulting eigenvalues $q_n$ define the **spectrum** of the observable (the outcome of possible measurements of $Q$) and its eigenstates represent **definite states** of $Q$ (those states where measuring $Q$ always returns the same outcome). 

!!! warning
    The eigenvalue problem $\hat{Q}\psi_n(x)=q_n\psi_n(x)$ has many solutions and not all of them will be physically meaningful. For example, only solutions leading to wave functions that are finite everywhere and that satisfies the relevant boundary conditions are physically acceptable.


### Properties of linear operators in Hilbert space

Let us now discuss the most important properties of these linear operators, $\hat{Q}$, which follow from the properties of operators acting on a general vector space.

- $\hat{Q} |\phi \rangle = |\psi \rangle$ where both $|\phi \rangle , |\psi \rangle \in \mathcal{H}$

- $\hat{Q}(c_1|\phi_1 \rangle + c_2 |\phi_2\rangle) = (c_1 \hat{Q}|\phi_1 \rangle + c_2 \hat{Q}|\phi_2\rangle)= (c_1 |\psi_1 \rangle + c_2|\psi_2\rangle);~~{\rm for}~c_1, c_2 \in \mathbb{C}$.

- $(\hat{Q}+\hat{P})|\phi \rangle = \hat{Q} |\phi \rangle + \hat{P} |\phi \rangle$.

- $(\hat{Q} \hat{P})|\phi \rangle= \hat{Q} (\hat{P})|\phi \rangle \ne  \hat{P} (\hat{Q})|\phi \rangle $

- $\hat{0}|\phi \rangle = 0, \forall \phi$ (null operator)

- $\hat{1}|\phi \rangle = |\phi \rangle,~~ \forall \phi$ (identity operator)

A crucial property of operators is that in general they will **not commute**, that is 

$$ \left( \hat{Q} \hat{P}\right) |\phi \rangle \neq \left(\hat{P} \hat{Q} \right)|\phi \rangle,~~ \forall |\phi \rangle $$

The reason is the same as why linear operators acting on vector spaces (**matrices**) do not commute between them. Therefore, when dealing with operators we must be careful since their order matters in determining the results of its action (as opposed to for example scalars).

We define the **commutator** between $\hat{Q}$ and $\hat{P}$ as

$$\left[\hat{Q},\hat{P}\right]\equiv \hat{Q}\hat{P}-\hat{P}\hat{Q}$$

If $[\hat{Q},\hat{P}]= 0$ we say that the two operators commute.
Note that the commutator of two operators is also an operator itself.

As we will see in the next lectures, if we have two observables $Q$ and $P$ whose corresponding operators do not commute, $[\hat{Q},\hat{P}]\ne0$, then $Q$ and $P$  will have associated a Heisenberg-type uncertainty principle which tells us that they cannot be **simultaneously measured**. We will call these pairs of observables as **incompatible**.

### Important operators

Let us present some important types of linear operators living in Hilbert spaces, which will be used in some of the subsequent lectures.

- *Inverse operator*: the operator inverse of a given operator $\hat{Q}$ is denoted by $\left(\hat{Q}\right)^{-1}$
  $$
  \forall |\phi \rangle,~~|\psi \rangle = \hat{Q} |\phi\rangle \Rightarrow |\phi\rangle = \left(\hat{Q}\right)^{-1} |\psi \rangle
  $$
  Clearly, the inverse operator satisfies $Q\left(\hat{Q}\right)^{-1}=\mathbb{1}$.

- *Adjoint or Hermitian conjugate operator*: the adjoint or Hermitian operator $\hat{Q}^{\dagger}$ is defined by
  $$
  \langle \phi | \hat{Q}^{\dagger} \psi \rangle = (\langle \psi | \hat{Q} \phi \rangle)^*
$$
  where $|\phi \rangle$ and $|\psi \rangle$ are vectors in the Hilbert space.

- *Hermitian operators*: if the adjoint operator $\hat{Q}^{\dagger}$ is equal to the operator itself $\hat{Q}$, then we call the operator Hermitian: $\hat{Q}^{\dagger}=\hat{Q}$. Hermitian operators are of huge importance in quantum mechanics, as we will discuss in the next lectures.

!!! important
    We will show that in quantum mechanics physical observables must be represented by Hermitian operators. These operators ensure that the outcome of a measurement of $Q$ is also a real quantity, and that the eigenvectors of $\hat{Q}$ represent a complete, orthonormal basis of the Hilbert space $\mathcal{H}$.

- *Unitary operator*: If the inverse of an operator is the adjoint operator, $\hat{U}^{-1}=\hat{U}^{\dagger}$, then this operator is called a unitary operator and $\hat{U}^{\dagger}\hat{U}=\hat{U}\hat{U}^{\dagger}=\hat{1}$

- *Projection operator*: the product between a ket and a bra vector is a projection vector: $\hat{P_n}=|\psi_n \rangle \langle \psi_n|$, that projects a given state $|\phi \rangle$ onto the vector $|\psi_n \rangle$,
  $$
  \hat{P}_n|\phi \rangle = |\psi_n \rangle \langle \psi_n|\phi \rangle = a_n|\psi_n \rangle \, ,\quad a_n=\langle \psi_n|\phi \rangle
  $$
  Note that if the state vector $|\phi \rangle$ is represented  by the orthonormal basis  $\{|\psi_n \rangle\}$ such that
  $$ |\phi\rangle  = \sum_m c_m |\psi_m \rangle $$
  then we have that the action of the projection operator is
  $$
  \hat{P}_n|\phi \rangle = \left( |\psi_n \rangle \langle \psi_n|\right)\sum_m c_m |\psi_m \rangle =c_n|\psi_n\rangle \, ,
  $$
  where one has used the orthonormality property of the basis vectors. By construction, projection operators are hermitian operators.




