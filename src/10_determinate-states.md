---
title: Hermitian operators
---

**Week 4, lecture 11**

# Determinate states and Hermitian operators

In the previous lecture, we have explained that the formalism of quantum mechanics is based on the linear algebra language, with state vectors describing our quantum systems and Hermitian linear operators representing actions (_e.g._ measurements) on them.

We are now going to discuss an important type of quantum states known as **determinate states**, which are those states for which the measurement of a given physical observables returns _always_ the same outcome. These states play an important role since, as we will shown, the eigenvectors of the Hermitian operator $\hat{Q}$ associated to the physical observable $Q$ correspond to the determinate states of this same observable $Q$.

This discussion will allow us to present a number of important properties of Hermitian operators and their associated eigenvalues and eigenvectors.

## Determinate states

To start the discussion, assume that we have an ensemble of quantum systems, all of them prepared in the same **identical** quantum state characterized by the state vector $|\psi \rangle$. 

If now we measure the value of a given physical observable $Q$ in each of these identical systems, can we expect to always find the same result, given that their state is the same?

This question might seem surprising, since in classical physics I will always obtain the same outcome if I repeat a given measurement in identical systems (neglecting the finite precision of the measurement). 

However, this is _not_ true in quantum physics: even if the quantum states $|\psi \rangle$ that form the ensemble are identical, in general I will find **different results** each time I measure the observable $Q$ in one of these states. 

!!! example
    Assume that we have a spin-1/2 particle whose state vector is given by $|\psi \rangle = a |\uparrow \rangle + b |\downarrow \rangle$ in terms of the basis eigenstates associated to _spin pointing up_ ($|\uparrow \rangle$) and _spin pointing down_  ($|\downarrow \rangle$) in the $z$ direction.  We prepare a large number of systems in this identical quantum state. Now if we measure the spin in the $z$-direction for each of these **identical systems**, sometimes we will measure $+\frac{\hbar}{2}$ and sometimes $-\frac{\hbar}{2}$, and it is impossible to **predict the outcome of individual measurements**.

Therefore for a general quantum state $|\psi \rangle$ it will not be possible to predict the outcome of individual measurements.

However, there are some special states for which a measurement of the observable $Q$ always returns the same result. We define the **determinate states** $|\psi \rangle$ of the observable $Q$ as those in which if we measure $Q$ we always the same result.

!!! example
    If $Q=E$ (the total energy) then the corresponding operator is $\hat{Q}=\hat{H}$ (the Hamiltonian operator). Assume that we have an ensemble of quantum harmonic oscillators, all of them in the ground state. Each time we measure the energy in one of these systems we will find the same result, $E_0=\hbar \omega/2$. This means that the ground state of the quantum harmonic oscillator is a determinate state of the energy $E$.

From the previous example, it is easy to see that any quantum state which solves the Schroedinger equation $\hat{H}|\psi \rangle=E|\psi \rangle$ will be a determinate state of the energy $E$.

!!! example
    Now let's consider again the spin of an electron. If the observable is the spin in the $z$ direction, $Q=S_z$, then the corresponding operator is $\hat{Q}=\hat{S_z}$ (which can be represented as a $2\times 2$ matrix. If the system  is in a quantum state is $|\psi \rangle = |\uparrow \rangle$, then this state corresponds to a determinate state of the spin in the $z$ direction $S_z$ since every measurement of $S_z$ in this quantum state returns $+\hbar/2$.

The previous example of the Schroedinger equation illustrates that, for a given physical observable $Q$, its determinate states will be the state vectors $|\psi\rangle$ that satisfy the corresponding **eigenvalue equation** for the Hermitian operator $\hat{Q}$:

$$\hat{Q}|\psi \rangle = q |\psi \rangle \, .$$ 

Note that in general the operator $\hat{Q}$ will have different eigenvectors, each of which representing a different determinate state of $Q$.

The **measurement axiom of quantum mechanics** then tells us that if a state vector $|\psi \rangle$ is an eigenvector of the Hermitian operator $\hat{Q}$ with eigenvalue $q$, then $|\psi \rangle$ is a determinate state of the observable $Q$ and every measurement of $Q$ in this state will always return its associated eigenvalue $q$.


Let us illustrate this important concept with an example.

!!! example
    Consider a quantum system composed by only two energy levels with associated state vectors $|\psi_1\rangle$ and $|\psi_2\rangle$:

    $|\psi_1\rangle$ $\rightarrow$ $E_1$ and $|\psi_2\rangle$ $\rightarrow$ $E_2$ 

    By construction, we know that $|\psi_1\rangle$ and $|\psi_2\rangle$ are determinate states of the total energy E, that is they are eigenstates of the Hamiltonian $\hat{H}$ with eigenvalues $E_1$ and $E_2$ respectively:

    $$\hat{H}|\psi_1\rangle = E_1|\psi_1\rangle$$

    $$\hat{H}|\psi_2\rangle = E_2|\psi_2\rangle$$

    Note that the **superposition principle** tells me that one can also construct valid quantum states for this system which are not determinate of the energy $E$.
    
    For instance, let me define $|\psi \rangle =\frac{1}{\sqrt{2}}|\psi_1\rangle-\frac{1}{\sqrt{2}}|\psi_2\rangle$. If we now apply the Hamiltonian operator $\hat{H}$ to this quantum state we find

    $$\hat{H}|\psi \rangle=\hat{H}\left(\frac{1}{\sqrt{2}}|\psi_1\rangle-\frac{1}{\sqrt{2}}\psi_2\rangle\right) = \frac{E_1}{\sqrt{2}}|
    \psi_1\rangle-\frac{E_2}{\sqrt{2}}|\psi_2\rangle \neq E |\psi \rangle.$$
    
    Here $|\psi \rangle$, despite being a superposition of determinate states of the energy, is not itself a determinate state.

This discussion illustrates that we can express the problem of measuring physics observables $Q$ in quantum mechanics as that of solving the eigenvalue equations for the corresponding Hermitian operator $\hat{Q}$. Indeed, determinate states are defined by the condition $\hat{Q}|\psi \rangle=q|\psi \rangle$ and there measurements of $Q$ will always return the same outcome $q$.

Taking this into account, one can see how it is very important to study the general properties of eigenvalue equations of the form $\hat{Q}|\psi \rangle=q|\psi \rangle$ when $\hat{Q}$ is an Hermitian operator.

## Properties of Hermitian operators (discrete spectra)

Quantum mechanics posits that that determinate states of $Q$ are eigenstates of the Hermitian operator $\hat{Q}$. 
Let us present a number of important properties of the eigenvalues and eigenfunctions of the Hermitian operators.

The starting point will be the eigenvalue equation associated to an Hermitian operator: 

$$\hat{Q}|\psi_n \rangle = q_n |\psi_n \rangle$$

where we account for the fact that in general this discussion will have multiple solutions.

As we have seen so far, the spectrum of an operator $\hat{Q}$ could be discrete (i.e. the quantum harmonic oscillator), continuous (i.e. the free particle), or a combination of a discrete and continuous (i.e. the delta function and the finite potential well, which exhibit both bound and scattering states).

Furthermore, the number of independent eigenvectors $|\psi_n \rangle$ can be finite (such as for the finite potential well) or infinite (such as for the quantum harmonic oscillator).

Let's discuss some relevant properties satisfied for the solutions of the eigenvalue equation for $\hat{Q}$ for the discrete case, and we will below consider how these are modified in the case that $\hat{Q}$ has associated a (partially) continuous spectra of eigenvalues and eigenfunctions.

1. The eigenvalues of an Hermitian operator $\hat{Q}$ are real ($q_n \in \mathbb{R}$).

    > *Proof*: Let's use the definition of an Hermitian operator, $\hat{Q}=\hat{Q}^{\dagger}$, and evaluate the following inner product:
    >
    > $$\langle \psi_n |\hat{Q}|\psi_n \rangle = \langle \psi_n |\hat{Q} \psi_n \rangle = q_n \langle \psi_n | \psi_n \rangle$$
    >
    > $$\langle \psi_n |\hat{Q}|\psi_n \rangle = \langle \psi_n \hat{Q}| \psi_n \rangle = q_n^* \langle \psi_n | \psi_n \rangle$$
    >
    > hence we obtain that
    >
    > $$ q \langle \psi_n | \psi_n \rangle = q^* \langle \psi_n | \psi_n \rangle$$
    >
    > therefore if a number is equal to its complex conjugate it must be real: $q_n \in \mathbb{R}$.

    Note that it could not have been otherwise: quantum mechanics tells us that the eigenvalues of $\hat{Q}$ are the only possible outcomes of measuring the observable $Q$, and certainly the outcome of a physical measurement must be a **real number**.

2. The eigenfunctions $|\psi_n \rangle$ associated to different eigenvalues $q_n$ of an Hermitian operator $\hat{Q}$ are **orthogonal** among them:

    $$\langle \psi_n |\psi_m \rangle=\delta_{nm} \, .$$

    >*Proof:* Let us consider two eigenfunctions of $\hat{Q}$ with different eigenvalues:
    >
    > $$\hat{Q}|\psi_1 \rangle = q_1 |\psi_1 \rangle$$
    >
    > and
    >
    > $$\hat{Q}|\psi_2 \rangle = q_2 |\psi_2 \rangle$$
    >
    > Since $\hat{Q}$ is Hermitian operator we have that, $\langle \psi_1 |\hat{Q}\psi_2 \rangle=\langle \hat{Q}\psi_1 |\psi_2 \rangle$ and this implies,
    >
    > $$q_2\langle \psi_1 |\psi_2 \rangle=q_1^*\langle \psi_1|\psi_2 \rangle$$
    >
    > But since we know that $q_1$ and $q_2$ are real, then the only way this equality is satistfied is if $\langle \psi_1|\psi_2 \rangle=0$.

    Therefore, $|\psi_1 \rangle$ and $|\psi_2 \rangle$ must be orthogonal provided that $q_1 \ne q_2$. This argument can be extended to all the eigenvalues of $\hat{Q}$.

    Note that we can always construct **normalised eigenvectors** by supplying them with the corresponding normalisation contants, and hence in general the eigenvectors of $\hat{Q}$ will be **orthonormal** among them.

3. The eigenstates of the operator $\hat{Q}$ associated to the observable $Q$ are **complete**. 

    This completeness property implies that any element of the Hilbert space $|\Psi\rangle$ can be expressed as a linear combination of these eigenstates. If $\hat{Q}|\psi_n \rangle = q_n |\psi_n \rangle$ then this property guarantees that

    $$|\Psi \rangle = \sum_n c_n |\psi_n \rangle$$

    where the complex coefficients $c_n$ can be expressed by computing the inner product

    $$ c_n = \langle \psi_n | \Psi \rangle\, .$$

## Properties of Hermitian operators (continuous spectra)

In the case of continous spectra, the discussion is a bit more subtle, since in general the state vectors **cannot be normalised**, as we saw in the case of the free particle for example. This means that the inner product might not exist, at least in the same sense as for the case of discrete spectra.

We need a different way to demonstrate that the properties of the Hermitian operator $\hat{Q}$ that we have found for the discrete case

    1.The eigenvalues are real.

    2.The eigenstates are orthogonal.

    3.The eigenvalues form a complete basis.

also apply for operators with continous spectra. We will do this via an specific example.

!!! important
    In the continuum case one can still mantain the concept of **orthogonality** between eigenvectors, but only if we modify this concept as compared to the case of operators with discrete spectra. In particular, we will need to replace the **Kronecker delta** $\delta_{mn}$ in the orthogonality condition by the **Dirac delta function** $\delta(x-y)$. 

Let us demonstrate with an example how the eigenvalues of $\hat{Q}$ are real and orthogonal also if $\hat{Q}$ has a (partially) continuous spectra.

!!! example "The eigenvales are real and orthogonal:"

    Say that we want to find the eigenvalues and eigenstates of the momentum operator $\hat{p}$. In the following we will work in the position representation, where $f_p(x)$ is the eigenfunction of the linear momentum operator.

    $$\hat{p}f_p(x)=pf_p(x)$$

    in this case the eigenvalue equation that we need to solve is:

    $$\frac{\hbar}{i}\frac{\partial}{\partial x} f_p(x)=p f_p(x)$$

    the general solution is: 

    $$f_p(x)=Ae^{i\frac{px}{\hbar}}$$

    which is **not normalizable** (it does not vanish for large $x$).

    Despite $f_p(x)$ being not normalizable,  one can attempt to calculate the inner product of two different eigenfunctions: $f_{p_1}(x)$ and $f_{p_2}(x)$.

    $$\langle f_{p_1}|f_{p_2}\rangle = \int_{-\infty}^{+\infty} dx |A|^2 e^{i\frac{(p_1-p_2)x}{\hbar}}=$$

    $$|A|^2\int_{-\infty}^{+\infty} dx\, e^{i\frac{(p_2-p_1)x }{\hbar}}$$

    It can be demonstrated that the linear momentum eigenvalues $p_1$ and $p_2$ are real, and hence the above inner product is given by

    $$ =2\pi \hbar |A|^2 \delta (p_2-p_1)$$

    We now choose $A=\frac{1}{\sqrt{2 \pi \hbar}}$ and we find that the eigenfunctions of the momentum operator $\hat{p}$ are given by

    $$f_p(x)=\frac{1}{\sqrt{2 \pi \hbar}}e^{i\frac{px}{\hbar}}$$
    
    and satifsy the following **orthogonality relation**,

    $$\langle  f_{p_1}|f_{p_2} \rangle =\delta (p_2-p_1)=\delta (p_1-p_2)\, .$$ 

    If $p_1 \neq p_2$ then $\delta (p_2-p_1)=0$, which is the analogue in the continuum case than the orthogonality relation in the discrete case.

    This relation $\langle f_{p_1}|f_{p_2} \rangle =\delta (p_2-p_1)$ is called **Dirac orthogonality**.

    An analogous relation can be obtained for other Hermitian operators with continuous spectra, such as the position operator $\hat{x}$

In the case of continuous spectra, in general it is not possible to demonstrate that the eigenfunctions of $\hat{Q}$ for a complete basis. 

It is often taken to be an **additional axiom** of quantum mechanics that only Hermitian operators with complete eigenvectors can represent physical observables, and hence all operators that we will consider in this course (either for discrete or continuous spectra) will have associated a complete basis of eigenfunctions.




