----
title: Finite Barrier
----

**Week 3, lecture 8**

!!! summary "Learning objectives"

    After following this lecture, you will be able to:
    
    - Identify the conditions that lead to bound and scattering states for a given potential.
    - Apply the relevant boundary conditions to specify the allowed wavefunctions and energies.
    - Recognise symmetries and be able to take advantage of them in order to choose the appropriate method for solving problem.
    - Represent graphically the wavefunctions in potentials that combine scattering and bound states.
    

# Finite square well and barrier

In the previous lecture, we have discussed an explicit example of a quantum system (the Dirac delta function potential) exhibiting both bound and scattering states.

In this lecture, we are going now to extend this discussion of scattering and bound states to more realistic (and important) configurations in quantum theory, namely systems characterised by either a **finite square well** or a **finite square barrier** potentials $V(x)$. 

We will first discuss in detail the case of the finite square well potential, and then show how the finite square barrier problem can be extrapolated from the previous case.

## Finite square well

The finite square well system is defined by the following potentital:

$$V(x) = \Bigg\{\begin{array}
{rrr}
-V_0 & {\rm for} & -\frac{a}{2}<x<\frac{a}{2}\\
0 & \qquad {\rm otherwise} &  \\
\end{array}
$$

where $V_0 > 0$ is a positive real constant that represents how deep is the potential well, and $a$ indicates the width of the well. Likewise, the finite square barrier system is defined by

$$V(x) = \Bigg\{\begin{array}
{rrr}
V_0 & {\rm for} & -\frac{a}{2}<x<\frac{a}{2}\\
0 & \qquad{\rm otherwise} &  \\
\end{array}
$$

where now $V_0$ represents the _height_ of the potential barrier. As mentioned above, in the following we will focus on the case of the square potential well and then later we will come back to the square barrier case. 

Below we display schematically the potential $V(x)$ in the case of the **finite square well**. Already at the qualitative levelm we can see that this quantum system will admit two types of solutions:

- For $E>0$ we will have **scattering states**, with a continuum of energies that are not bounded from above.  We have that $E>V(x)$ everywhere in space (classically allowed region) and hence for these scattering states we expect an oscillatory behaviour of the wave function.

- For $E<0$ we will have **bound states**, characterised by a discrete (quantized) energy spectrum and by normalized wavefunctions. 

Inside the potential barrier, we have that $E > V(x)$ (solid purple line) and hence we expect an oscillatory behaviour for the wave function. For $|x|>a/2$ instead, we are in the classically forbidden regime (dashed purple line) where the wave function is expected to decay exponentially fast and vanish for $x \to \pm \infty$.

![image](figures/finite_well_potential_1.png)

As in the discussion of the delta potential, we start with the bound states and then move to the corresponding scattering states.

### Bound states in the finite square well

Let's start by discussing bound states in the potential well. For the schematic of the potential $V(x)$ displayed below, we can observe that these bound states will have energies in the range $-V_0 < E < 0$. Therefore in the following we consider only solutions whose energies correspond to the situation below, where the solid (dashed) purple line indicates the classically allowed (forbidden) region of $x$. 

![image](figures/finite_well_potential_2.png) 

The first step in order to solve the Schroedinger equation in this system is to divide the problem into different regions, with each region having associated a different value of the potential. 

It should be clear  from the schematic that we need to define three regions: first a region such that $x<-a/2$ where $V(x)=0$, then the potential well region characterised by $V(x)=-V_0$, and finally the right part of the potential well $x>a/2$ where again $V(x)=0$. These three regions are thus defined by the conditions

$${\rm Region~I}:~~x<-\frac{a}{2}$$

$${\rm Region~II}:~~-\frac{a}{2}\le x \le \frac{a}{2}$$

$${\rm Region~III}:~~x>\frac{a}{2}$$

and are indicated in the schematic below.

![image](figures/finite_well_potential_4.png) 

Let us now solve the Schroedinger equation separately in each of the three regions, and relate these solutions by means of the boundary conditions that the wave function of the system must satisfy. 

!!! Recall 
    First, the wave function $\psi(x)$ must be **continuous** everywhere, so in our case we will impose its continuity at $x=-a/2$ and $x=a/2$. Second, we also know that for _finite potentials_ (as in this case) also the first derivative of the wave function must be continuous, and hence we will impose that $d\psi(x)/dx$ is continuous at both $x=-a/2$ and $x=a/2$. 

- ${\rm Region~I}:~~x<-\frac{a}{2}$:

Here the Schroedinger equation reduces to that of the free particle given that $V(x)=0$. Therefore the equation that we need to solve in this region is given by

$$-\frac{\hbar^2}{2m}\frac{d^2\psi_I(x)}{dx^2}=-|E|\psi_I(x);~~{\rm Eq.}~[1]$$

where we have written $-|E|$ in the right hand side (RHS) to make explicit the requirement that the energy must be negative there (recall that we are dealing with bound states now). 

The most general solution of Eq. [1] will be given by

$$\psi_I(x)=Ae^{kx}+Be^{-kx}\quad {\rm where} \quad k\equiv\sqrt{\frac{2m|E|}{\hbar^2}}>0$$

where as usual $A$ and $B$ are integration constants that will be fixed by the continuity boundary conditions on the wave function. Note that here we have real (rather than imaginary) exponentials in $\psi_I(x)$, as expected given that Region I corresponds to a classically forbidden region.

Furthermore, we know that bound states must have _integrable wave functions_. To make sure that $\psi_I(x)$ is finite at $x\rightarrow -\infty$, we must impose that $B=0$. Therefore, we end up finding that the wavefunction in Region I is given by 

$$\psi_I(x)=Ae^{kx}~~{\rm Eq.}~[2]$$

- ${\rm Region~III}:~~x>\frac{a}{2}$:

The Schroedinger equation to be solved in Region III is exactly the same as in Region I, Eq. [1], since in both cases the potential vanishes V(x)=0. Therefore we will have that

$$\psi_{III}(x)=Fe^{kx}+Ge^{-kx}\quad {\rm where} \quad k\equiv\sqrt{\frac{2m|E|}{\hbar^2}}$$

and to ensure that the wavefuntion vanishes when $x\rightarrow\infty$ (which is required to make the wave function of this bound state normalisable) then we need to impose that $F=0$ and hence we find that in Region III the wave function is given by

$$\psi_{III}(x)=Ge^{-kx};~~{\rm Eq.~}[3]$$

with the exponential suppresion characteristic of the quantum wave functions in classically forbidden regions.

- ${\rm Region~~II}:~~-\frac{a}{2} \le x \le \frac{a}{2}$:

Finally, we need to tackle Region II, corresponding to the inside of the potential well, and where the energy satisfies $E > V(x)$. The Schroedinger equation to be solved in this region is the following:

$$-\frac{\hbar^2}{2m}\frac{d^2\psi_{II}(x)}{dx^2}+(-V_0)=-|E|\psi_{II}(x)$$

and moving $V_0$ to the RHS of the equation

$$-\frac{\hbar^2}{2m}\frac{d^2\psi_{II}(x)}{dx^2}=(-|E|+V_0)\psi_{II}(x);~~{\rm Eq.~[4]}$$

Since $V_0$ is positive and $E > (-V_0)$ (see the schematics above) it follows that $(-|E|+V_0)>0$, and thus you can verify that the most general solution to Eq. [4] will be given by 

$$\psi_{II}(x)=Ce^{ilx}+De^{-ilx} \quad {\rm where} \quad l\equiv\sqrt{\frac{2m(-|E|+V_0)}{\hbar^2}}>0$$

which displays an oscillatory behaviour, rather than the real exponentials that characterised the wave functions in Regions I and III. 

!!! Recall
    Using the expression of the cosinus and the sinus in terms of complex exponentials, $\cos(x)=(e^{ix}+e^{-ix})/2$ and $\sin(x)=(e^{ix}-e^{-ix})/2i$ you can show that a linear combination of $\sin(x)$ and $\cos(x)$ can always be expressed as a linear combination of $e^{ix}$ and $e^{-ix}$.

We know that a linear combination of imaginary exponentials can be rewritten as a linear combinations of sinus and cosinus with the same argument; we use this property to express the solution to Eq. [4]: as

$$\psi_{II}(x)=C\cos(lx)+D\sin(lx);~~{\rm Eq.~}[5].$$

This transformation, as we will show below, greatly facilitates the physical intepretation of the wave function in this region. 

Note also that Eq. [5] is the sum of a _symmetric function_ ($\cos(-lx)=\cos(lx)$) and of an _antisymmetric function_ ($\sin(-lx)=-\sin(lx)$). This observation will become handy in the following.

We can now put together the wave functions that we have derived in the three regions of this problem:

$$\psi_I(x)=Ae^{kx}\,;~~x<(-a/2);~~{\rm Eq.}~[2]$$ 

$$\psi_{II}(x)=C\cos(lx)+D\sin(lx)\,;~~-a/2 \le x \le a/2;~~{\rm Eq.~}[5]$$

$$\psi_{III}(x)=Ge^{-kx}\,;~~x>a/2;~~{\rm Eq.~}[3]$$

We see that the solutions obtained depend on four unknown integration constants $A, C, D$ and $G$. Fortunately, imposing the boundary conditions mentioned above we may be able to fix these some of these constants, since we have two relations coming from imposing the continutity of the wavefunction at $x=\pm a/2$ and two more from imposing the same condition for the first derivative.

- **Continuity** of the wavefunction:

$$\psi_{I}(x=-a/2)=\psi_{II}(x=-a/2);~~{\rm Eq.~}[6]$$

$$\psi_{II}(x=a/2)=\psi_{III}(x=a/2);~~{\rm Eq.~}[7]$$

From Eq. [6] we derive the following relation between $A, C$, and $D$: 

$$ Ae^{-ka/2}=C\cos(la/2)-D\sin(la/2);~~{\rm Eq.~[8]}$$

where note that we have used the symmetry (antisymmetry) property of the cosinus (sinus).

Before moving forward, we note that the potential function of the finite well problem is **symmetric**, $V(−x)=V(x)$, in other words, it is an _even function_ of the position $x$. This property has an important consequence, which is fully general and applies also beyond this specific system. 

!!! info     
    A **symmetric potential** $V(x)$ (such that $V(-x)=V(x)$) imposes certain conditions on the symmetry properties of the wavefunction. If we consider the Schroedinger equation $$ -\frac{\hbar^2}{2m} \frac{d^2\psi(x)}{dx^2} + V(x)\psi(x) = E\psi(x)$$ and now we transform from $x$ to $-x$ and use that the potential is symmetric we get that $$ -\frac{\hbar^2}{2m} \frac{d^2\psi(-x)}{dx^2} + V(x)\psi(-x) = E\psi(-x)$$ we can conclude that the solutions of this equation must have associated well-defined symmetry properties upon a $x \to -x$ transformation: they must be either _even functions_ in the position $x$, $\psi(-x)=\psi(x)$, or instead _odd functions_ of the position, $\psi(-x)=-\psi(x)$. Only function with those properties can be solutions of the Schroedinger equation with a symmetric potential.

In our case, since the square well potential is symmetric in Region II, we know that the wave functions in this region, Eq. [5], must be either **even** or **odd** functions of $x$:  $\psi_{II}(-x)=\pm \psi_{II}(x)$. 

Now, this is why it was a good idea to express $\psi_{II}(x)$ in terms of sinus and cosinus: for the even solutions, $\psi_{II}(-x)=\psi_{II}(x)$, we will have that $D=0$ (since the sinus is an odd function) while conversely for the odd solutions, $\psi_{II}(-x)=-\psi_{II}(x)$, we will have $C=0$ since the cosinus is even.

In the case of the _even solutions_, imposing $D=0$ reduces Eq. [8] to

$$Ae^{-ka/2}=C\cos(la/2);~~{\rm Eq.~[9]}$$

and likewise in this case Eq. [7] is simplified to

$$C\cos(la/2)=Ge^{-ka/2};~~{\rm Eq.}~[10]$$

We will come back later to the case of the odd solutions, for the time being we continue our derivation of the equations imposed by the boundary conditions for the case of even wavefunctions.

- **Continuity of the first derivative** of the wavefunction:

Next we impose the continuity of the first derivative of the wavefunction at $x=\pm a/2$, which must be satisfied given that the potential $V(x)$ is finite everywhere. Imposing these two conditions:

$$\frac{d\psi_I}{dx}(x=-a/2)=\frac{d\psi_{II}}{dx}(x=-a/2);~~{\rm Eq.~}[11]$$

$$\frac{d\psi_{II}}{dx}(x=a/2)=\frac{d\psi_{III}}{dx}(x=a/2);~~{\rm Eq.~}[12]$$

and recalling that we are considering here only even solutions (hence $D=0$) we obtain the following two equations:

$$Ake^{-ka/2}=-Cl\sin(-la/2)=Cl\sin(la/2);~~{\rm Eq.}~[13]$$

$$-Cl\sin(la/2)=-Gke^{ka/2};~~{\rm Eq.}~[14]$$

At this point we have four unknowns and four equations, and hence it is a system of equations that we should be able to solve. 

Let us put together the four equations that we have derived from the boundary conditions, Eqns. [9], [10], [13], and [14]:

$$Ae^{-ka/2}=C\cos(la/2);$$

$$C\cos(la/2)=Ge^{-ka/2};$$

$$Ake^{-ka/2}=Cl\sin(la/2);$$

$$Cl\sin(la/2)=Gke^{-ka/2};~$$

From the first two equations we have that $A=G$ must hold, which then implies that the third and fourth equation are identical. Therefore, the only two independent equations that we have available are:

$$Ae^{-ka/2}=C\cos(la/2);$$

$$Ake^{-ka/2}=Cl\sin(la/2);$$

and if now we divide the first equation with respect to the second one we find that $k^{-1}=l^{-1}(1/\tan(la/2))$, which can also be expressed as

$$\tan(la/2)=\frac{k}{l};~~k=\sqrt{\frac{2m|E|}{\hbar^2}}; l=\sqrt{\frac{2m(-|E|+V_0)}{\hbar^2}};~~{\rm Eq.}~[14]$$

which does not depend on any integration constants. 

The solutions of this equation, for a given potential $V_0$, determine the values of the energy $E$ of the bound states of the system. Since, as we will see next, Eq. [14] admits  an infinite set of solutions but these are separated in energy, implying that we have also demonstrated that the bound states in the finite square well system are **quantized**. 

This equation cannot be solved analytically so instead we need to resort to numerical methods. To do so, it is convenient to introduce the following dimensionless variables:

$$u\equiv\frac{la}{2};~~v\equiv\frac{ka}{2};~~u_0=\sqrt{\frac{2ma^2V_0}{\hbar^2}}$$

which as you can check are related by $u^2=u^2_0-v^2$. 

In terms of these variables, Eq. [14] reads

$$\tan(u)=\sqrt{\left(\frac{u_0}{u}\right)^2-1};~~{\rm Eq.}~[15]$$

which can be solved numerically. One possibility is to represent graphically the tangent function $\tan(u)$ together with $\sqrt{\left( \frac{u_0}{u}\right)^2-1}$. As indicated in the graphic below (where a value of $u_0=10$ is used),  the points where these two curves cross correspond to the values of the energy $E$ that lead to solutions of Eq. [14]. In the graphic below, we indicate with vertical dashed lines the asymtotae of the tangent function. 

![image](figures/tangent.png)

By using some numerical software, one can determine the values of $u$ and hence of $E$ for which the two functions cross and thus solve Eqns. [14] and [15]. The specific numerical values of these solutions are not particularly instructive, but it is instead useful to consider the limiting cases where the potential well is very deep, since it allows us to reproduce known results.

!!! note
    There is a very interesting limit of the previous result. In the limit of either a **very deep** ($V_0 \to \infty$) or **very wide** $(a\to \infty)$ potential well, we have that $u_0\to \infty$ and you can verify that the intersections occur at values of $u$ just below the asymptotae of the tangent, that is

    $$u_n \simeq n\pi/2 \,, \quad \to \quad E_n + V_0 \simeq \frac{n^2\pi^2\hbar^2}{2ma^2} \, , \quad n=1,3,5,7,\ldots$$

    You can convince yourselves that this result is identical to that of the [infinite square well](https://qm1.quantumtinkerer.tudelft.nl/4_ISW/) of width $a$, which is not surprising since we are taking the $V_0\to \infty$ limit. In this limit we have an infinite number of bound states, though for finite $V_0$ the number of bound states will be finite. The _missing_ solutions of the infinite square well ($n=2,4,6,\ldots$) will come from the _odd_ solutions that are discussed below.

Another interesting limit is that of a narrow ($a\to 0$) or shallow ($V_0 \to 0$) well for which $u_0\to 0$. From the schematic above, we see that as $u_0$ decreases we will have less and less crossings (implying a reduction in the number of bound states), down to the limit where there is a _single bound state_ (for $u_0 < \pi/2$).

In the figure below we show schematically the wavefunction $\psi(x)$ for the bound state of the potential well with the lowest energy. Note how it is indeed even under the $x\to -x$ transformation and how it falls off exponentially for large $|x|$ in the classically forbidden region. 

![image](figures/finite_well_potential_wavefunction.png)

Bound states with higher energy will display oscillations with higher frequencies in the well region, $-a/2 \le x \le a/2$.

Finally, we can extend the previous derivation to the case of the **odd solutions** of the Schroedinger equation in the region $-a/2 \le x \le a/2$, for which $C=0$ and the wave function in the region inside the potential well is given by

$$\psi_{II}(x)=D\sin(lx)\,;~~-a/2 \le x \le a/2;~~{\rm Eq.~}[5]$$

Imposing now the continuity of the wave function and of its first derivative at $x=\pm a/2$ we end up with the following four equations:

$$ Ae^{-ka/2}=-D\sin(la/2)$$

$$ Ge^{-ka/2}=D\sin(la/2)$$

$$ Ake^{-ka/2}=lD\cos(la/2)$$

$$-Gke^{-ka/2}=lD\cos(la/2)$$

Comparing the first two equations we get that $A=-G$, which makes the third and fourth equation identical. So, as in the case of the odd solutions, we end up with two independent equations, which can be conbined to yield

$$\tan(la/2)=-l/k$$

and using the same change of variables as for the even solutions we end up with the following equation:

$$ \tan(u) = -1/\sqrt{ (u_0/u)^2-1 } $$

which again can be solved numerically (and reproduces the missing solutions of the infinite potential well in the limit $u_0\to \infty$). You can see below a graphical representation of this equation, with the crossings indicating the allowed values of the energy $E$ for the odd solutions of this system.

![image](figures/tangent_odd.png) 

You can verify how after each even solution comes an odd solutions and viceversa. In the limit $V_0 \to \infty$ the even (odd) solutions appear at $u=\pi/2, 3\pi/2, 5\pi/2,\ldots$ ($u=\pi, 2\pi, 3\pi,\ldots$)


### Scattering states in the finite square well

Following this discussion of the bound states of the square potential well, we move to describe the the corresponding scattering states, defined as those states for which $E>0$ as indicated in the schematic below:

![image](figures/finite_well_potential_3.png) 

Clearly for such states we have $E>V(x)$ everywhere and thus we expect to find oscillatory solutions for the wave function.

As we did in the case of the bound states, the first step is divide the problem into different regions, with each region representing a different potential. These three regions will be the same as in the previous case.  Subsequently, we need to solve the Schroedinger equation separately in each of the three regions:

${\rm Region~I}:~~x<-a/2$

In this region the potential vanishes, $V(x)=0$, and making explicit that the energy $E >0$ is positive we end up with the following equation

$$-\frac{\hbar^2}{2m}\frac{d^2\psi_{I}(x)}{dx^2}=|E|\psi_{I}(x)$$

which is of course nothing but the free particle Schroedinger equation that we know how to solve:

$$\psi_{I}(x)=Ae^{ikx}+Be^{-ikx};~~k=\sqrt{\frac{2m|E|}{\hbar^2}}$$

${\rm Region~II}:~~-a/2 \le x \le a/2$

Also here the Schroedinger equation can be expressed in a way that resembles the free-particle case:

$$-\frac{\hbar^2}{2m}\frac{d^2\psi_{II}(x)}{dx^2}=(|E|+V_0)\psi_{II}(x)$$

and now recalling that $V(x)$ is symmetric and thus the wave functions in this region will be either _even_ or _odd_ under a transformation of the form $x \to -x$ we can express its solution as

$$\psi_{II}(x)=C\cos(lx)+D\sin(lx);~~l\equiv\sqrt{\frac{2m(|E|+V_0)}{\hbar^2}}$$

where $D=0$ for even solutions and $C=0$ for odd solutions, in exactly the same manner as in the case of the bound states.

${\rm Region~III}:~~x>a/2$

Now again we end up with the free particle equation

$$-\frac{\hbar^2}{2m}\frac{d^2\psi_{III}(x)}{dx^2}=|E|\psi_{III}(x)$$

$$\psi_{III}(x)=Fe^{ikx}+Ge^{-ikx};~~k=\sqrt{\frac{2m|E|}{\hbar^2}}$$

In a similar way as in the case of the [scattering states for the delta potential](https://qm1.quantumtinkerer.tudelft.nl/7_scattering_states/#scattering-states), the interpretation of this solution is the following (see also the schematic below):

- The component $Ae^{ikx}$ represents a particle propagating from the left towards the potential well, and the component $Fe^{ikx}$ represents a particle moving towards the right way from the potential well.

- The component $Be^{-ikx}$ represents a particle propagating from the right away from the potential well, and the component $Ge^{-ikx}$ represents a particle moving from the left towards the potential well.

- In the well region $-a/2 \le x \le a/2$ we have either even oscillatory functions ($C\cos(lx)$) or odd oscillatory functions ($D\sin(lx)$)

![image](figures/deltafunction_scattering_scheme_updated.png)

Now, as in the case of the delta function, the physically interesting case is that of a particle approaching the potential well from the left. In this configuration, $Ae^{ikx}$, $Fe^{ikx}$, and $Be^{-ikx}$ can be understood as the **incident**, **transmitted**, and **reflected** waves, and we can then set $G=0$. 

We can now write down the complete expression for the wave function of the finite square well for scattering states ($E>0)$ assuming an incident particle approaching the barrier from the left:

$$\psi_{I}(x)=Ae^{ikx}+Be^{-ikx};~~k =\sqrt{\frac{2m|E|}{\hbar^2}}$$

$$\psi_{II}(x)=C\cos(lx)+D\sin(lx);~~l =\sqrt{\frac{2m(|E|+V_0)}{\hbar^2}}$$

$$\psi_{III}(x)=Fe^{ikx};~~k=\sqrt{\frac{2m|E|}{\hbar^2}}$$

As we have done in the previous cases, we can try to determine the integration constants $A, B, C, D$, and $F$ by means of the boundary conditions that must be satisfied by the wave function and its derivative.

- **Continuity** of the wavefunction:

$$\psi_I(x=-a/2=\psi_{II}(x=-a/2);~~{\rm Eq.}~[16]$$

$$\psi_{II}(x=a/2)=\psi_{III}(x=a/2);~~{\rm Eq.~}[17]$$

From Eq. [16] we have $Ae^{-ika/2}+Be^{ika/2}=C\cos(la/2)-D\sin(la/2)$

From Eq. [17] we have $C\cos(-la/2)-D\sin(la/2)=Fe^{ika/2}$

- **Continuity of the first derivative** of the wavefunction:

$$\frac{d\psi_I}{dx}(x=-a/2)=\frac{d\psi_{II}}{dx}(x=-a/2);~~{\rm Eq.~}[18]$$

$$\frac{d\psi_{II}}{dx}(x=a/2)=\frac{d\psi_{III}}{dx}(x=a/2);~~{\rm Eq.~}[19]$$

From Eq. [18] we have $ikAe^{-ika/2}-ikBe^{ika/2}=lC\sin(la/2)+lD\cos(la/2)$

From Eq. [19] we have $ikFe^{ika/2}=-lC\sin(la/2)+lD\cos(la/2)$

By doing some algrebra we can rearrange these equations to obtain the following result:

$$F=\frac{ e^{-ika} }{ \cos(la)-i\frac{(k^2+l^2)}{2kl}\sin(la) }A$$

$$B=i\frac{\sin(la)}{2kl}(l^2-k^2)F$$

In analogy with the case of the scattering states in the delta function potential, we can define a **transmission** and a **reflection** coefficients by dividing the square of the coefficients of the transmitted and reflected waves respectively by that of the incident wave.

The transmission coefficient $T$ will then be given by

$$T \equiv \frac{|F|^2}{|A|^2}=\left( 1+ \frac{V^2_0}{4E(E+V_0)} \sin^2\left( \frac{a}{\hbar}\sqrt{2m(E+V_0)}\right)\right)^{-1}$$

As expected, $T$ ranges between 0 and 1 and its value will depend on the interplay between the particle energy $E$ and the depth and width $V_0$ and $a$ of the potential well.

!!! info
    From the previous result, you can verify that for certain values of the values of the energy $E$  the probability of transmission when crossing the potential well will be unity, $T=1$, which corresponds to a _perfect transmission_ without any reflection. For these values of the energy, the potential well becomes effectively **transparent**. The condition for $T=1$ is satisfied when $$\frac{a}{\hbar}\sqrt{2m(E+V_0)}=n\pi$$ with $n$ an integer number, which correspond to values of the energy
    
    $$E_n+V_0=\frac{n^2\pi^2\hbar^2}{2ma^2}$$
    
    which is the same result as the energies of an infinite square well with width $a$.

## Finite square barrier

We now move to discuss the case of the **finite square barrier** potential. Recall that this system is defined by a potential $V(x)$ of the from

$$V(x) = \Bigg\{\begin{array}
{rrr}
V_0 & {\rm for} & -\frac{a}{2}<x<\frac{a}{2}\\
0 & \qquad{\rm otherwise} &  \\
\end{array}
$$

where $V_0$ is a positive real number representing the _height_ of the potential barrier and $a$ indicates its width. This potential is represented graphically in the figure below:

![image](figures/finite_barrier_onlypotential.png)

Inspection of this potential indicates that one important difference as compared to the case of the finite well is that here we cannot have bound states, and hence _all allowed states will be scattering states_. The reason is that we cannot have states with negative energy, and hence all states will have $E>0$ implying that for $x\to \pm \infty$ we have that $E > V(x)$ with the oscillatory-like wave function characteristic of the scattering states.

This strategy to solve the Schroedinger equation for the finite barrier follows closely that of the finite well. Here we only sketch the main ingredients of the derivation and discuss the qualitative results expected, and leave the complete derivation as an exercise for the student.

Even if only scattering states are allowed, we need to consider two different situations:

- First, when the energy is *smaller* than the height of the barrier, $0 < E < V_0$. At the classical level, a particle with energy $E$ below the height of a potential barrier $E_0$ would never the able to cross it. 

    However, as we will show,    this is not the case in quantum theory, and an incident particle will have a _finite probability_ to cross the barrier. This phenomenon is known as **quantum tunneling**.

- Second, when the energy of the particle is *larger* than the height of the barrier, $ E > V_0$. In this situation, classically we expect the particle to be able to cross the barrier, and in quantum theory we will see that there is indeed some probability for **transmission** across the barrier but also there is a finite probability for **reflection**.

Let us now discuss these two situations in turn.

### The finite barrier for E<V0

Let us consider first the case for which the particle energy satisfies $0 \le E \le V_0$, which corresponds to the configuration depicted below. In this figure we also represent a characteristic wave function associated to this configuration, we will now motivate the behaviour that it exhibits.

![image](figures/finite_barrier_potential_higherenergy.png)

Assuming that we have an incident wave approaching the barrier from the left, you can verify that the solution of the Schroedinger equation in the three usual regions is given by (recall that in this system all energies will be positive):

$$\psi_I(x)=Ae^{ikx}+Be^{-ikx}\quad {\rm where} \quad k\equiv\sqrt{\frac{2mE}{\hbar^2}}>0$$

$$\psi_{II}(x)=Ce^{lx}+De^{-lx}\quad {\rm where} \quad k\equiv\sqrt{\frac{2m(E-V_0)}{\hbar^2}}>0$$

$$\psi_{III}(x)=Fe^{ikx}$$

The integration constants $A, B, C, D$, and $F$ are related by the boundary conditions stemming from the continuity of the wave function and its derivative at $x=\pm a/2$. We see how in Region III the wave function oscillates with the same frequency as in Region I but with a _smaller amplitude_ due to the fact that the barrier will reflect part of the incident wave function. In Region III, the classically forbidden region, the probability of finding the particle is _non-zero_ but exponentially suppressed. 

By analogy with the case of the scattering states in the potential well, we can compute the transmission coefficient $T=|F|^2/|A|^2$ finding that

$$ T^{-1} = 1 + \frac{V_0^2}{4E(V_0-E)}\sinh^2 \left( \frac{a}{\hbar}\sqrt{2m(V_0-E)}\right)$$

By recalling the properties of the hyperbolic sinus $\sinh(x)$, we note that in the limit $V_0 \gg 0$ (where the potential barrier becomes infinitely steep) then $T\to 0$, implying that there is no transmitted wave and that all the incident well is reflected by the potential barrier.

Conversely, if either $a\to 0$ or $V_0\to 0$, a limit in which the barrier vanishes, we obtain perfect transmission $T\to 1$ as expected.

!!! note
    The fact that for $E < V_0$ we get a non-zero transmission coefficient, $T\ne 0$, is a most remarkable phenomenon of quantum theory known as **tunneling**. This means that in quantum mechanics, a particle can cross barriers in a way that is not possible in classical physics. However, we note that the probability $T$ for tunneling is exponentially suppressed and becomes negligible unless the barrier is very narrow or the energy $E$ close to $V_0$.

### The finite barrier for E>V0

Finally, we consider schematically the case for which the particle energy satisfies $E \ge V_0$, which corresponds to the configuration depicted below. In this figure we also represent a characteristic wave function associated to this configuration. 

![image](figures/finite_barrier_potential_2.png)

By proceeding in the usual way (solving the Schroedinger equation in each region and implementing the boundary conditions) you can convince yourselves that the qualitative behaviour of the wave function $\psi(x)$ is what it should be because of the following reasons:

- In Regions I and III, the oscillation frequency of the wave is the same (same argument of $e^{\pm ikx}$).

- In Region II, the oscillation frequency decreases since $l < k$ in the argument of the exponentials.

- In Region III, the amplitude of the oscillation is suppressed as compared to that of the incident wave since only part of the latter is transmitted and the rest is reflected.

## Questions

1. A beam of electrons with total energy $E$ is incident on the potential steps from the left.

    **(a)** Will the beam be reflected in case A and case B? Justify your answer.

    **(b)** Let $R_A$ and $R_B$ denote the reflection coefficients in A and B. Is $R_A$ greater, smaller, or equal to $R_B$? What about $R_C$ compared to $R_D$? Justify your answer.

2. Two beams of electrons, A and B, with total energy of E and 2E respectively, are incident on the potential barrier from the left.

    **(a)** Sketch to show qualitatively the waveefunction of the two beams in all three regions. How is the wave function of the incident beam A similar to the wave function of beam B? How is it different?

    **(b)** Suppose the length of the barrier is increased to 2L. How will you answers be changed? How is the wave function. of beam. A in region III similar to the. wave function of beam A in region III when the length of the barrier is still L? How is it different? And how about beam B? Justify your answer.

3. Consider an electron in the potential well shown below. $E_1$ is the first excited state energy.

    **(a)** Sketch the first excited state wave function. Justify your answer.

    **(b)** Sketch the wave function corresponding $E_2$. Justify your answer.

    **(c)** In both cases, where will the electron be most likely found? In each case, are there any regions that the electron will never be found? Justify your answer.
