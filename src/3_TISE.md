---
title: The time-independent Schrödinger Equation
---

# The Schrödinger equation

**Week 1, Lecture 3**


So far we have been working with the time-dependent Schrödinger equation, which is a partial differential equation

$$i\hbar\frac{\partial\Psi}{\partial t}=-\frac{\hbar^2}{2m}\frac{\partial^2\Psi}{\partial x^2} + V\cdot\Psi$$

In general, the potential in which the particle sits will depend on time, i.e. $V=V(x,t)$. Let's now assume for a moment, that the potential is stable in time $V(x,t)=V(x)$. While this seems very limiting at first, it's actually quite a common case. We can then separate the variables by introducing two new functions $\Psi(x,t)=\psi(x)\cdot\phi(t)$ and write the TDSE as

$$i\hbar\psi(x)\frac{\mathrm{d}\phi(t)}{\mathrm{d} t}=-\frac{\hbar^2}{2m}\frac{\mathrm{d}^2\psi(x)}{\mathrm{d} x^2}\phi(t) + V(x)\cdot\psi(x)\phi(t)$$

We have no more partial derivates and we can re-arrange the equation such that the time dependent parts are on the left and the position dependent on the right

$$i\hbar\frac{1}{\phi(t)}\frac{\mathrm{d}\phi(t)}{\mathrm{d} t}=-\frac{\hbar^2}{2m}\frac{1}{\psi(x)}\frac{\mathrm{d}^2\psi(x)}{\mathrm{d} x^2} + V(x)=\mathrm{constant}=E$$

As the two sides depend on different variables and are equal to one another, the only possible solution is a constant, which we call $E$. The position dependent part is called the time-independent Schrödinger equation (TISE):

$$-\frac{\hbar^2}{2m}\frac{\mathrm{d}^2\psi(x)}{\mathrm{d} x^2} + V(x)\psi(x)=E\psi(x)$$

while the time-dependent part simplifies to

$$\frac{\mathrm{d}\phi(t)}{\mathrm{d}t}=-\frac{iE}{\hbar}\phi(t)$$

We ended up with two ordinary differential equations instead of one partial. As a next step, we will try to find a general solution:

1. For the time-dependent part we guess the simple, general solution to be related to $e^{ax}$, as it takes the right form $\frac{\mathrm{d}}{\mathrm{d}x}e^{ax}=a\cdot e^{ax}$. Hence the general solution is $\phi(t)=e^{-iEt/\hbar}$.

2. In order to solve the TISE we will need to specify the potential $V(x)$. This is exactly what we will do for the rest of the course: choose a specific $V(x)$ and then solve the TISE.


!!! Note
    In order to make the distinction between variables and operators clear, we will from now on use "hats" on operators: $\hat{x}$, $\hat{p}$, etc.


The time-independent Schrödinger equation is an eigenvalue differential equation, with the set of solutions $\psi_n(x)$ and eigenvalues $E_n$. These seperable solutions have some interesting properties:

1. They are stationary states, which means "nothing" except for the wavefunction changes in time. While $\Psi(x,t)=\psi_n(x)e^{-iE_nt/\hbar}$ is time dependent, $|\Psi(x,t)|^2$ is not. This is easy to see:

    $$|\Psi(x,t)|^2=\psi_n^*e^{iE_nt/\hbar}\psi_ne^{-iE_nt/\hbar}=|\psi_n(x)|^2$$

2. The solutions have **no** uncertainty in their total energy! From classical mechanics you are familiar with the energy $E=\frac{1}{2}mv^2+V=\frac{p^2}{2m}+V(x)=H(x,p)$, which is also called the Hamiltonian. In quantum mechanics we have something similar: remember the operator for momentum $\hat{p}=\frac{\hbar}{i}\frac{\mathrm{d}}{\mathrm{d}x}$, which we can use to write the total energy as

    $$\hat{E}=\frac{1}{2m}\left(\frac{\hbar}{i}\frac{\mathrm{d}}{\mathrm{d}x}\right)\left(\frac{\hbar}{i}\frac{\mathrm{d}}{\mathrm{d}x}\right)+V(x)=\frac{-\hbar^2}{2m}\frac{\mathrm{d}^2}{\mathrm{d}x^2}+V(x)=\hat{H}$$

    Here $\hat{H}$ is the Hamiltonian in QM and the TISE then simplifies to $\hat{H}\psi_n=E_n\psi_n$. Now, in order to demonstrate that the solutions $\psi_n$ have no uncertainty in energy, we need to calculate the standard deviation $\sigma_E$ of the energy for $\psi_n$: $\sigma_E=\sqrt{\langle E^2\rangle-\langle E\rangle^2}$.

    $$\langle E\rangle=\int_{-\infty}^{+\infty} \psi_n^*(x)e^{iE_nt/\hbar}\hat{H}\psi_ne^{-iE_nt/\hbar} \,\mathrm{d}x = \int_{-\infty}^{+\infty} \psi_n^*(x)E_n\psi_n(x) \,\mathrm{d}x = $$

    $$=E_n \int_{-\infty}^{+\infty} \psi_n^*(x)\psi_n(x) \,\mathrm{d}x = E_n$$

    $$\langle E^2\rangle=\int_{-\infty}^{+\infty} \psi_n^*(x)\hat{H}^2\psi_n \,\mathrm{d}x=\int_{-\infty}^{+\infty} \psi_n^*(x)E_n^2\psi_n \,\mathrm{d}x=E_n^2$$

    $$\sigma_E^2=\langle E^2\rangle-\langle E\rangle^2=E_n^2-E_n^2=0$$

    Hence $\psi_n$ have definite energy!

3. The expectation value of any operator $\hat{Q}$ is independent of time.

    $$\langle Q\rangle=\int_{-\infty}^{+\infty} \Psi*(x,t)\hat{Q}\left(x,\frac{\mathrm{d}}{\mathrm{d}x}\right)\Psi(x,t)\,\mathrm{d}x =$$

    $$=\int_{-\infty}^{+\infty} \psi_n^*(x)e^{iE_nt/\hbar}\hat{Q}\left(x,\frac{\mathrm{d}}{\mathrm{d}x}\right)\psi_n(x)e^{-iE_nt/\hbar}\,\mathrm{d}x =$$

    $$=\int_{-\infty}^{+\infty} \psi_n^*(x)\hat{Q}\left(x,\frac{\mathrm{d}}{\mathrm{d}x}\right)\psi_n(x)\,\mathrm{d}x$$

4. The solutions $\psi_n$ form a complete and orthonormal set:
    - We can perform a Fourier decomposition of any arbitrary function: $f(x)=\sum_n c_n \psi_n(x)$
    - $\psi_n$ are normalized: $\int_{-\infty}^{+\infty} \psi_n^*\psi_n \,\mathrm{d}x = 1$
    - $\int_{-\infty}^{+\infty} \psi_n^*\psi_m \,\mathrm{d}x = \delta_{n,m}$, where $\delta_{n,m}$ is the [Kronecker delta](https://en.wikipedia.org/wiki/Kronecker_delta).


!!! Note
    Recipe for solving the TDSE with $V=V(x)$

    - Step 1: expand $\Psi(x,0)=\sum_n c_n\psi_n(x)$
        $\int_{-\infty}^{+\infty} \psi_m^*\Psi(x,0) \,\mathrm{d}x = \int_{-\infty}^{+\infty} \psi_m^*\sum_n c_n\psi_n(x) \,\mathrm{d}x = \sum_n c_n\int_{-\infty}^{+\infty} \psi_m^*\psi_n(x) \,\mathrm{d}x = \sum_n c_n \delta_{m,n} = c_m$
        $\rightarrow c_n=\int_{-\infty}^{+\infty}\psi_n^*\Psi(x,0)\,\mathrm{d}x$
    
    - Step 2: add the time-dependence of $\psi_n$. If $\Psi_n(x,t=0)=\psi_n(x) \rightarrow \Psi_n(x,t)=e^{-iE_nt/\hbar}\psi_n(x)$

    - Step 3: use the superposition principle $\Psi(x,t)=\sum_n c_n e^{-iE_nt/\hbar}\psi_n(x)$


!!! check "Example:"
    A particle is prepared in a superposition state (i.e. the linear combination of stationary states): $\Psi(x,0)=c_1\psi_1+c_2\psi_2$. We know that $\psi_1$ and $\psi_2$ are stationary states with energies $E_1$ and $E_2$, respectively.
    
    - What is $\Psi(x,t)$? $\Psi(x,t)=c_1e^{-iE_1t/\hbar}\psi_1+c_2e^{-iE_2t/\hbar}\psi_2$

    - What is $|\Psi(x,t)|^2$?

    $$|\Psi(x,t)|^2=\left(c_1^*e^{iE_1t/\hbar}\psi_1^*+c_2^*e^{iE_2t/\hbar}\psi_2^*\right)\left(c_1e^{-iE_1t/\hbar}\psi_1+c_2e^{-iE_2t/\hbar}\psi_2\right)=$$

    $$=|c_1|^2|\psi_1|^2+|c_2|^2|\psi_2|^2+c_1c_2\left(e^{i(E_1-E_2)t/\hbar}+e^{-i(E_1-E_2)t/\hbar}\right)\psi_1\psi_2=$$

    $$=c_1^2\psi_1^2+c_2^2\psi_2^2+2c_1c_2\cos\left(\frac{(E_1-E_2)t}{\hbar}\right)\psi_1\psi_2=$$

    For simplicity, we have assumed $c_1$, $c_2$ and $\psi_1$, $\psi_2$ to be real.

The $|c_n|^2$ gives the probability of measuring energy $E_n$. This is immediately clear if $\Psi=\psi_1$ and for a superposition state $\Psi=c_1\psi_1+c_2\psi_2$ it implies that $\sum_n|c_n|^2=1$, i.e. normalization. It turns out that for any $\Psi=\sum c_n\psi_n$, the expectation value $\langle E\rangle$ is independent of time. This is of course the familiar conservation of energy! Please note that a measurement *does not* preserve energy.
